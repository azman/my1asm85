;*******************************************************************************
; monitor program for 8085 development board
; - memory> ROM:0000H-1FFFH , RAM:2000H-3FFFH
;*******************************************************************************

cr:			equ		0dh
lf:			equ		0ah
nil:		equ		00h
space:		equ		20h
ram:		equ		2000h
ramsize:	equ		2000h
sysregs:	equ		3ff0h
intvecs:	equ		3fd0h
s_stack:	equ		3ff0h
u_stack:	equ		3fd0h
baudtime:	equ		18
baudtime1:	equ		19

			; it begins...
			org 0000h ; pure system restart (RST0)
			jmp init
			; define system handlers
			org 0008h ; graceful system restart (RST1)
			jmp user_exit
			org 0010h ; serial transmit (RST2)
			jmp tx_char
			org 0018h ; serial receive (RST3)
			jmp rx_char
			org 0020h ; user-defined (RST4)
			jmp rst_h4
			org 0024h ; ISR (TRAP)
			jmp isr_trap
			org 0028h ; user-defined (RST5)
			jmp rst_h5
			org 002ch ; ISR (RST5.5)
			jmp isr_55
			org 0030h ; user-defined (RST6)
			jmp rst_h6
			org 0034h ; ISR (RST6.5)
			jmp isr_65
			org 0038h ; user-defined (RST7)
			jmp rst_h7
			org 003ch ; ISR (RST7.5)
			jmp isr_75

			; initialize monitor program
			org 0040h
init:		lxi sp,s_stack ; set system stack
			lxi h,u_stack
			shld stacku ; default user stack
			lxi h,ram
			shld pcaddr ; user code address
			mvi a,11000000b
			sim ; serial output idle (1), interrupt mask disable
			call wait ; let it wait a bit! :p

			; main interface routine
menu:		lxi h,str_menu
			call put_str
main:		lxi h,str_prompt
			call put_str
			; get & check user selection
			call getecho
			cpi "L"
			jz user_load
			cpi "E"
			jz user_exec
			cpi "D"
			jz user_disp
			cpi "S"
			jz user_subs
			cpi "R"
			jz user_regs
			cpi "I"
			jz show_ivec
			cpi "V"
			jz show_info
			cpi "*"
			jz show_wave
			cpi "M"
			jz menu
			jmp main

; routine to just wait

wait:		push b
			mvi b,255
wait2:		mvi c,255
wait1:		dcr c
			jnz wait1
			dcr b
			jnz wait2
			pop b
			ret

; routine to send null-terminated string through serial

put_str:	mov a,m
			cpi nil
			rz ; return when done!
			call tx_char
			inx  h
			jmp put_str

; routine to send 1 char through serial

tx_char:	push h
			push b
			mov c,a         ;4
			mvi b,10        ;7
			mvi a,01000000b ;7
			sim             ;4
tx_char2:	mvi h,baudtime  ;7
tx_char1:	dcr h           ;4
			jnz tx_char1    ;7/10
			mov a,c         ;4
			stc             ;4
			rar             ;4
			mov c,a         ;4
			rar             ;4
			ani 80h         ;7
			ori 01000000b   ;7
			sim             ;4
			dcr b           ;4
			jnz tx_char2    ;7/10
			pop b
			pop h
			ret             ;10

; routine to get and echo user input

getecho:	push b
			call rx_char
			mov b,a
			call tx_char ; do echo?
			mov a,b
			; ensure uppercase input
			cpi 61h ; 'a'
			jc getechoX
			cpi 7bh ; 'z' + 1
			jnc getechoX
			sui 20h
getechoX:	pop b
			ret

; routine to get 1 char from serial

rx_char:	push h
			push b
			mvi b,9           ;7
rx_char1:	rim               ;4
			ora a             ;4
			jm rx_char1       ;7/10
			mvi h,baudtime1/2 ;7
rx_char2:	dcr h             ;4
			jnz rx_char2      ;7/10
rx_char4:	mvi h,baudtime1   ;7
rx_char3:	dcr h             ;4
			jnz rx_char3      ;7/10
			rim               ;4
			ral               ;4
			dcr b             ;4
			jz rx_charX       ;7/10
			mov a,c           ;4
			rar               ;4
			mov c,a           ;4
			jmp rx_char4      ;10
rx_charX:	mov a,c           ;4
			pop b
			pop h
			ret

; [CODE] save reg values for further inspection & return to monitor prog

user_exit:	shld regsHL ; store HL
			pop h ; get PC
			shld pcaddr ; store PC
			push psw
			pop h
			shld regsFA ; store FA
			push b
			pop h
			shld regsBC ; store BC
			push d
			pop h
			shld regsDE ; store DE
			lxi h,0
			dad sp
			shld stacku ; store SP
			lhld stacks ; restore monitor prog's SP (saved before exec)
			sphl
			jmp main

; [CODE] load user program

user_load:	lxi h,str_load
			call put_str
ldx0:		call rx_char ; getecho?
			cpi ":"
			jnz ldx0
			call getdata ; get length
			cpi 00h
			jz ldx2
			mov c,a
			call getdata ; get address high
			mov h,a
			call getdata ; get address low
			mov l,a
			call getdata ; get record type / ignore for now
			; cpi ??? ; only process record type ???
			; jnz ldx0
ldxx:		call getdata ; get code
			mov m,a ; write to memory
			inx h
			dcr c
			jnz ldxx
			jmp ldx0
ldx2:		mvi c,10 ; zero-length line has exactly 5 bytes?
ldx3:		call rx_char ; getecho?
			dcr c
			jnz ldx3
			lxi h,str_done
			call put_str
			jmp main

; routine to get 1 hex-ascii data byte from serial

getdata:	push b
			call rx_char ; getecho?
			call asc2bin
			ral
			ral
			ral
			ral
			mov b,a
			call rx_char ; getecho?
			call asc2bin
			ora b
			pop b
			ret

; routine to convert hex-ascii to binary

asc2bin:	cpi 30h ; '0'
			jc asc2binX
			cpi 3ah
			jnc asc2bin0
			sui 30h
			ret
asc2bin0:	cpi 41h ; 'A'
			jc asc2binX
			cpi 47h
			jnc asc2bin1
			sui 40h
			adi 09h
			ret
asc2bin1:	cpi 61h ; 'a'
			jc asc2binX
			cpi 67h
			jnc asc2binX
			sui 60h
			adi 09h
			ret
asc2binX:	mvi a,0ffh
			ret

; [CODE] execute user program

user_exec:	lxi h,0
			dad sp
			shld stacks ; save monitor program's stack pointer
			; do this everytime... just to be sure
			mvi a,31h ; opcode for lxi sp
			sta user
			lxi h,u_stack
			shld stacku ; default user stack
			mvi a,0c3h ; opcode for jmp
			sta code
			lxi h,str_cad
			call put_str
			call getaddr
			shld pcaddr
			lxi h,str_run
			call put_str
			; load register value in case modified by user!
			lhld regsFA ; load FA
			push h
			lhld regsBC ; load BC
			push h
			lhld regsDE ; load DE
			push h
			lhld regsHL ; load HL
			pop d
			pop b
			pop psw
			jmp user

; routine to receive address from user (return in hl)

getaddr:	push d
			push b
			lxi h,nil
			mvi e,0
getaddr0:	call getecho
			cpi cr
			jz getaddrX
			inr e
			call asc2bin
			mov c,a
			mvi b,0
			dad h
			dad h
			dad h
			dad h
			dad b
			jmp getaddr0
getaddrX:	pop b
			mov a,e
			pop d
			ora a
			rnz ; return if more than one input char
			pop psw
			jmp main

; routine to receive byte from user

getbyte:	push b
			push d
			mvi b,nil
			mvi e,0
getbyte0:	call getecho
			cpi cr
			jz getbyteX
			inr e
			call asc2bin
			mov c,a
			mov a,b
			ral
			ral
			ral
			ral
			add c
			mov b,a
			jmp getbyte0
getbyteX:	mov a,e
			pop d
			ora a
			jz getbyteZ
			mov a,b
			pop b
			ret
getbyteZ:	pop b
			pop psw
			jmp main

; [CODE] dump memory content

user_disp:	lxi h,str_mad
			call put_str
			call getaddr
disp1:		call put_newln
			call put_newln
			mvi c,08h
disp3:		mov a,h
			call putbyte
			mov a,l
			call putbyte
			mvi a,":"
			call tx_char
			call put_space
			mvi b,10h
disp2:		mov a,m
			call putbyte
			call put_space
			inx h
			dcr b
			jnz disp2
			call put_newln
			dcr c
			jnz disp3
			xchg
			lxi h,str_end
			call put_str
			xchg
			call getecho
			cpi "Q"
			jz main
			jmp disp1

; routine to put a byte on terminal (as hex ascii)

putbyte:	push h
			push d
			push b
			mov d,a
			rrc
			rrc
			rrc
			rrc
			ani 0fh
			mov c,a
			mvi b,0
			lxi h,hextbl
			dad b
			mov a,m
			call tx_char
			mov a,d
			ani 0fh
			mov c,a
			mvi b,0
			lxi h,hextbl
			dad b
			mov a,m
			call tx_char
			pop b
			pop d
			pop h
			ret

; routines to put newline & whitespace on terminal

put_newln:	mvi a,cr
			call tx_char
			mvi a,lf
			call tx_char
			ret

put_space:	mvi a,space
			call tx_char
			ret

put_modsym: mvi a," "
			call tx_char
			mvi a,"="
			call tx_char
			mvi a,">"
			call tx_char
			mvi a," "
			call tx_char
			ret

; [CODE] modify memory content

user_subs:	lxi h,str_mad
			call put_str
			call getaddr
subs0:		call put_newln
			call put_newln
			mov a,h
			call putbyte
			mov a,l
			call putbyte
			mvi a,"-"
			call tx_char
			mov a,m
			call putbyte
			call put_modsym
			call getbyte
			mov m,a
			inx h
			jmp subs0

; [CODE] modify register contents

user_regs:	call put_newln
			call show_regs
			lxi h,str_mod
			call put_str
			call getecho
			cpi "Y"
			jnz main
			call put_newln
			mvi c,8 ; 8 x 8-bit registers to show
			lxi d,regsFA + 1 ; starts with F... downwards
			lxi h,str_regF
usregs0:	call put_newln
			call put_str
			inx h ; up to the next!
			xchg
			mov a,m
			call putbyte
			call put_modsym
			call getbyte
			mov m,a
			dcx h ; down to the next!
			xchg
			dcr c
			jnz usregs0
			jmp main

; routine to to display registers

show_regs:	push b
			push d
			push h
			lxi h,str_reg
			call put_str
			call put_newln
			mvi c,8 ; 8 x 8-bit registers to show
			mvi b,4 ; newline break after 4
			lxi d,regsFA + 1 ; starts with F... downwards
			lxi h,str_regF
shregs0:	call put_str
			inx h ; up to the next!
			xchg
			mov a,m
			call putbyte
			dcx h ; down to the next!
			xchg
			dcr b
			jnz shregs1
			call put_newln
shregs1:	dcr c
			jnz shregs0
			call put_newln
			; now show the other two: SP and PC
			call put_str
			inx h ; up to the next!
			xchg
			lxi h,stacku + 1 ; get high byte!
			mov a,m
			call putbyte
			dcx h
			mov a,m
			call putbyte
			xchg
			call put_str
			lxi h,pcaddr + 1
			mov a,m
			call putbyte
			dcx h
			mov a,m
			call putbyte
			call put_newln
			pop h
			pop d
			pop b
			ret

; routine to display interrupt table

show_ivec:	lxi h,str_inttab
			call put_str
			jmp main

; routine to display version info

show_info:	lxi h,str_vers
			call put_str
			jmp main

; routine to generate test squarewave at all 8255 ports

show_wave:	lxi h,str_wav
			call put_str
			mvi a,80h
			out 83h
			mvi a,0ffh
show_wave0:	inr a
			out 80h
			out 81h
			out 82h
			call wait
			jmp show_wave0

; DATA AREA!

hextbl:		dfb "0","1","2","3","4","5","6","7","8","9"
			dfb "A","B","C","D","E","F"

str_vers:	dfb cr,lf,cr,lf
			dfb "Version: "
NAME:		dfb " MY1MON85 "
			dfb "-"
VERS:		dfb " 1.0.0 ",cr,lf
			dfb "Author: azman@my1matrix.org",cr,lf
			dfb "Note: Based on code by ARMS",cr,lf,nil

str_menu:	dfb cr,lf,cr,lf
			dfb "--------------------------------------",cr,lf
			dfb "8085 Microprocessor Development System",cr,lf
			dfb "--------------------------------------",cr,lf
			dfb "L - Load User Program (Intel HEX file)",cr,lf
			dfb "E - Execute Program",cr,lf
			dfb "D - Display Memory Contents",cr,lf
			dfb "S - Subtitute Memory Contents",cr,lf
			dfb "R - Display & Modify Register Contents",cr,lf
			dfb "I - Interrupt Table",cr,lf
			dfb "M - Menu",cr,lf,nil

str_prompt:	dfb cr,lf,cr,lf,"8085> ",nil

str_load:	dfb cr,lf,cr,lf,"Receiving HEX file...",nil

str_done:	dfb cr,lf,cr,lf,"Load Completed!",cr,lf,nil

str_cad:	dfb cr,lf,"Enter code address: ",nil

str_run:	dfb cr,lf,"Program running...",nil

str_mad:	dfb cr,lf,"ROM address:0000 - 1FFFH",cr,lf
			dfb "RAM address:2000 - 3FFFH",cr,lf
			dfb cr,lf,"Enter Address(Hex): ",nil

str_reg:	dfb cr,lf,"Current register contents",cr,lf,nil

str_end:	dfb cr,lf,"<press any key to continue or Q to quit>",nil

str_mod:	dfb cr,lf,"Modify Register? (y/N)",nil

str_wav:	dfb cr,lf,cr,lf,"Test counter running on all 8255 Ports...",nil

str_regF:	dfb "  F: ",nil
str_regA:	dfb "  A: ",nil
str_regB:	dfb "  B: ",nil
str_regC:	dfb "  C: ",nil
str_regD:	dfb "  D: ",nil
str_regE:	dfb "  E: ",nil
str_regH:	dfb "  H: ",nil
str_regL:	dfb "  L: ",nil
str_regSP:	dfb "  SP: ",nil
str_regPC:	dfb "  PC: ",nil

;str_err: 	dfb cr,lf,cr,lf,"Error!",cr,lf,nil

str_inttab:	dfb cr,lf,cr,lf
			dfb "**********************",cr,lf
			dfb "Interupt Table Address",cr,lf
			dfb "**********************",cr,lf
			dfb "RST4   - 3FE0H",cr,lf
			dfb "TRAP   - 3FE4H",cr,lf
			dfb "RST5   - 3FE8H",cr,lf
			dfb "RST5.5 - 3FECH",cr,lf
			dfb "RST6   - 3FF0H",cr,lf
			dfb "RST6.5 - 3FF4H",cr,lf
			dfb "RST7   - 3FF8H",cr,lf
			dfb "RST7.5 - 3FFCH",cr,lf,nil

			org sysregs
stacks:		dfs 2
regsHL:		dfs 2
regsDE:		dfs 2
regsBC:		dfs 2
regsFA:		dfs 2
user:		dfs 1
stacku:		dfs 2
code:		dfs 1
pcaddr:		dfs 2

			org intvecs
rst_h4:		dfs 4
isr_trap:	dfs 4
rst_h5:		dfs 4
isr_55:		dfs 4
rst_h6:		dfs 4
isr_65:		dfs 4
rst_h7:		dfs 4
isr_75:		dfs 4

			end

Note: just to show that after END directive, my1asm85 ignores EVERYTHING!
