# makefile for my1asm85 - a cross-platform assembler for 8085 microprocessor

PROJECT = my1asm85
TOOLOBJ = my1ihex.o my1symbol.o my1instruction.o my1code.o my1parser.o
TOOLOBJ += my1strchk.o my1code85.o $(PROJECT).o
TOOLPRO = $(PROJECT)
VERSION ?= $(shell cat VERSION)
PLATBIN ?= $(shell uname -m)
MON85LBL = monitor85
MON85SRC = asm/$(MON85LBL).asm

HEXTOOL = ihex_tool
HEXOBJS = my1ihex.o my1ihex_test.o

DELETE = rm -rf

CFLAGS += -Wall
LFLAGS += -static
OFLAGS +=

ifeq ($(DO_MINGW),YES)
	TOOLPRO = $(PROJECT).exe
	XTOOL_DIR	?= /home/share/tool/mingw
	XTOOL_TARGET	= $(XTOOL_DIR)
	CROSS_COMPILE	= $(XTOOL_TARGET)/bin/i686-pc-mingw32-
	PLATBIN = mingw
	CFLAGS += -I$(XTOOL_DIR)/include -DDO_MINGW
	LFLAGS += -L$(XTOOL_DIR)/lib
endif
ifeq ($(OS),Windows_NT)
	TOOLPRO = $(PROJECT).exe
	PLATBIN = mingw
	CFLAGS += -I$(XTOOL_DIR)/include -DDO_MINGW
	LFLAGS += -L$(XTOOL_DIR)/lib
endif

CC = $(CROSS_COMPILE)gcc

pro: CFLAGS += -DPROGVERS=\"$(VERSION)\"
debug: CFLAGS += -DMY1DEBUG -g
mon: MONL=$(shell cat $(MON85SRC) | grep NAME: | sed 's|^.*\" \(.*\) \".*|\1|')
mon: MONV=$(shell cat $(MON85SRC) | grep VERS: | sed 's|^.*\" \(.*\) \".*|\1|')

.PHONY: all new clean debug pro mon

pro: $(TOOLPRO)

hex: $(HEXTOOL)

all: pro hex

new: clean pro

mon: pro
	./$(TOOLPRO) --hex --list $(MON85SRC)
	mv $(MON85LBL).hex $(MONL)-$(MONV).hex
	mv $(MON85LBL).lst $(MONL)-$(MONV).lst

tool: new
	strip --strip-unneeded $(TOOLPRO)

debug: new

$(TOOLPRO): $(TOOLOBJ)
	$(CC) -o $@ $+ $(LFLAGS) $(OFLAGS)

$(HEXTOOL): $(HEXOBJS)
	$(CC) -o $@ $+ $(LFLAGS) $(OFLAGS)

%.o: src/%.c src/%.h
	$(CC) -c $(CFLAGS) $<

%.o: src/%.c
	$(CC) -c $(CFLAGS) $<

clean:
	-$(DELETE) $(TOOLPRO) $(HEXTOOL) *.o *.exe *.hex *.lst
