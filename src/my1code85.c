/*----------------------------------------------------------------------------*/
#include "my1code85.h"
#include "my1strchk.h"
#include "my1codedb8085.h"
/*----------------------------------------------------------------------------*/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
/*----------------------------------------------------------------------------*/
int get_reg8(char *pstr, byte08_t *bcod, int shif) {
	int loop, test = SYNTAX_INVALID_PARAM;
	if (!pstr) return SYNTAX_NO_PARAM;
	for (loop=0;loop<REGCOUNT;loop++) {
		if (strcmp(pstr,REG8085[loop])==0) {
			bcod[0] |= ((byte08_t) loop & 0xFF) << shif;
			test = SYNTAX_OK;
			break;
		}
	}
	return test;
}
/*----------------------------------------------------------------------------*/
int get_reg16(char *pstr, byte08_t *bcod, int shif, int stackop) {
	int loop, test = SYNTAX_INVALID_PARAM, offs = 0, stop = RGPCOUNT;
	if (!pstr) return SYNTAX_NO_PARAM;
	/* variations of reg pair */
	if (stackop>0) offs = RGPCOUNT;
	else if (stackop<0) stop = RGPCOUNT>>1;
	for (loop=0;loop<stop;loop++) {
		if (!strcmp(pstr,RGP8085[loop+offs])) {
			bcod[0] |= ((byte08_t) loop & 0xFF) << shif;
			test = SYNTAX_OK;
			break;
		}
	}
	return test;
}
/*----------------------------------------------------------------------------*/
int get_strdata(char *pstr, byte08_t *bcod, my1inst_t *pcod) {
	int test = SYNTAX_OK, loop, size, init;
	if(!pstr) return SYNTAX_NO_PARAM;
	size = strlen(pstr);
	if (pstr[0]!='"'||pstr[size-1]!='"')
		return SYNTAX_NOT_STR;
	if (!pcod) {
		if (size!=3) return SYNTAX_INVALID_PARAM;
		bcod[0] = (byte08_t) pstr[1];
		return test;
	}
	init = pcod->size;
	loop = init + size - 2;
	inst_resize(pcod,loop);
	if (pcod->size!=loop) return SYNTAX_MEMORY_ERROR;
	for (loop=1;loop<size-1;loop++,init++)
		pcod->data[init] = (byte08_t)pstr[loop];
	return test;
}
/*----------------------------------------------------------------------------*/
int get_ivalue(char *pstr, byte08_t *bcod, int limit16) {
	int test = SYNTAX_INVALID_PARAM, length, temp, loop, limit = 0xFF;
	word16_t *wcod = (word16_t*) bcod; /* NOT on PowerPC! */
	if (!pstr) return SYNTAX_NO_PARAM;
	if (limit16) limit = 0xFFFF;
	length = strlen(pstr);
	switch (pstr[length-1]) {
		case 'H':
			pstr[length-1] = 0x0;
			if (sscanf(pstr,"%x",&temp)==1)
				test = SYNTAX_OK;
			break;
		case 'B':
			pstr[length-1] = 0x0;
			for (temp=0,loop=0;loop<length-1;loop++) {
				temp <<= 1;
				if(pstr[loop]=='1') temp++;
				else if(pstr[loop]!='0') break;
			}
			if(loop==length-1) test = SYNTAX_OK;
			break;
		case 'O':
			pstr[length-1] = 0x0;
			if (sscanf(pstr,"%o",&temp)==1)
				test = SYNTAX_OK;
			break;
		default:
			if (sscanf(pstr,"%d",&temp)==1)
				test = SYNTAX_OK;
	}
	if(temp>limit) return SYNTAX_INVALID_PARAM;
	if (limit16) wcod[0] |= ((word16_t) temp & limit);
	else bcod[0] |= ((byte08_t) temp & limit);
	return test;
}
/*----------------------------------------------------------------------------*/
int get_rstn(char *pstr, byte08_t *bcod) {
	int test = SYNTAX_INVALID_PARAM, temp;
	if (!pstr) return SYNTAX_NO_PARAM;
	if (sscanf(pstr,"%d",&temp)==1) {
		if (temp>=0&&temp<=7) {
			bcod[0] |= ((byte08_t) temp & 0xFF) << 3;
			test = SYNTAX_OK;
		}
	}
	return test;
}
/*----------------------------------------------------------------------------*/
int get_expression(char *pstr, byte08_t *bcod, my1code_t *code) {
	int loop, index, temp = 0, value = 0, test = SYNTAX_OK;
	char this_op = EXPR_OPS_ADD;
	char *ptoken, *pnext, *exprbuff = 0x0, *ptest;
	word16_t *wcod = (word16_t*) bcod; /* NOT on PowerPC! */
	if (!pstr) return SYNTAX_NO_PARAM;
	/* process in uppercase */
	str2upper(pstr);
	/* check for label */
	if (is_label_init(*pstr))
		test = SYNTAX_CHECK_LABEL;
	/* check for expressions */
	if (get_operator(pstr,(char*)EXPR_OPS,&temp))
		test = SYNTAX_CHECK_EXPR;
	/* not in this category! */
	if (test==SYNTAX_OK)
		return SYNTAX_NOT_EXPR;
	if (temp>0) {
		/* create temp processing buffer - 'expand' operators */
		exprbuff = malloc(strlen(pstr)+1+(2*temp));
		for (index=0,loop=0;loop<strlen(pstr);loop++) {
			if (is_operator(pstr[loop],(char*)EXPR_OPS)) {
				exprbuff[index++] = ' ';
				exprbuff[index++] = pstr[loop];
				exprbuff[index++] = ' ';
			}
			else exprbuff[index++] = pstr[loop];
		}
		exprbuff[index] = 0x0;
		pnext = exprbuff;
	}
	else {
		/* assign arg for next strtok */
		pnext = pstr;
	}

	/* browse through label/expression */
	do {
		/* get token */
		ptoken = strtok(pnext, TOK_DELIM);
		pnext = 0x0; temp = 0;
		/* check if should get operator instead */
		if (this_op==EXPR_OPS_NOT) {
			/* okay... done? */
			if (!ptoken) {
				test = SYNTAX_OK;
				break;
			}
			/* operator strlen should be 1 */
			if (strlen(ptoken)!=1) {
				test = SYNTAX_NO_PARAM;
				break;
			}
			/* get operator */
			ptest = get_operator(ptoken,(char*)EXPR_OPS,0x0);
			if (ptest) this_op = *ptest;
			else {
				test = SYNTAX_INVALID_OPS;
				break;
			}
			/* get next operand */
			continue;
		}
		/* expecting an operand */
		if (!ptoken) {
			test = SYNTAX_NO_PARAM;
			break;
		}
		/* check if token is a label? */
		if (is_label_init(*ptoken)) {
			test = codx_find_symbol(code,(byte08_t*)&temp,ptoken);
			if (test<0) {
				if (code->flag&FLAG_MUST_RESOLVE) {
					test = SYNTAX_MUST_RESOLVE_ADDR;
				}
				else if (code->flag&FLAG_PASS_2) {
					test = SYNTAX_INVALID_LABEL;
					break;
				}
				else if (code->flag&FLAG_PASS_1) {
					test = SYNTAX_OK;
					break;
				}
			}
		}
		else {
			test = get_ivalue(ptoken, (byte08_t*) &temp, 1);
		}
		/* break on error? */
		if (test<0) break;
		/* execute operation */
		switch (this_op) {
			case EXPR_OPS_ADD: value += temp; break;
			case EXPR_OPS_SUB: value -= temp; break;
			case EXPR_OPS_MUL: value *= temp; break;
			case EXPR_OPS_DIV: value /= temp; break;
			/* should never be in here! */
			default: test = SYNTAX_INVALID_PARAM; break;
		}
		if (test<0) break;
		/* reset ops */
		this_op = EXPR_OPS_NOT;
	} while (1);
	if (test==SYNTAX_OK) *wcod = (word16_t) value;
	if (exprbuff) free((void*)exprbuff);
	return test;
}
/*----------------------------------------------------------------------------*/
int codx_opcode(my1code_t* code, char *parg, int pick) {
	my1inst_t* pcod;
	int loop, done = 0, step = 0, stat = SYNTAX_OK;
	char *pchk[MAX_ARG_CHECK] = { 0x0, 0x0, 0x0 };
	byte08_t bcod[MAX_CODEBYTE] = { 0x0, 0x0, 0x0 };
	/* prepare arguments */
	if (parg) {
		pchk[0] = strtok(parg,ARG_DELIM);
		for (loop=1;loop<MAX_ARG_CHECK;loop++) {
			pchk[loop] = strtok(0x0,ARG_DELIM);
		}
	}
	/* false do-while loop - for error exits */
	do {
		/* assign main codebyte */
		bcod[INDEX_OPCODE] = CodeDB[pick].code;
		/* should not be more than 2 argument? */
		while (step<MAX_OPCODE_ARG) {
			/* trim whitespace */
			if (pchk[step]) trimws(pchk[step],0x0,1);
			/* check argument */
			switch(CodeDB[pick].arg[step]) {
				case ARGTYPE_REG1:
					stat = get_reg8(pchk[step],&bcod[INDEX_OPCODE],3);
					break;
				case ARGTYPE_REG2:
					stat = get_reg8(pchk[step],&bcod[INDEX_OPCODE],0);
					break;
				case ARGTYPE_RGP1:
					stat = get_reg16(pchk[step],&bcod[INDEX_OPCODE],4,0);
					break;
				case ARGTYPE_RGP2:
					stat = get_reg16(pchk[step],&bcod[INDEX_OPCODE],4,1);
					break;
				case ARGTYPE_RGP3:
					stat = get_reg16(pchk[step],&bcod[INDEX_OPCODE],4,-1);
					break;
				case ARGTYPE_IMM8:
					stat = get_strdata(pchk[step],&bcod[INDEX_IMM08],0x0);
					if (stat!=SYNTAX_NOT_STR) break;
					stat = get_expression(pchk[step],&bcod[INDEX_IMM08],code);
					if (stat!=SYNTAX_NOT_EXPR) break;
					stat = get_ivalue(pchk[step],&bcod[INDEX_IMM08], 0);
					break;
				case ARGTYPE_DI16:
					stat = get_expression(pchk[step],&bcod[INDEX_IMM08],code);
					if (stat==SYNTAX_NOT_EXPR)
						stat = get_ivalue(pchk[step],&bcod[INDEX_IMM08],1);
					break;
				case ARGTYPE_RSTX:
					stat = get_rstn(pchk[step],&bcod[INDEX_OPCODE]);
					break;
				case ARGTYPE_NONE:
					/* end of arg indicator */
					done = 1;
					break;
				default: stat = SYNTAX_NO_PARAM; break;
			}
			if (stat<0||done) break;
			step++;
		}
		/* because of the 2-layer loop */
		if (stat<0) break;
		/* check extra param */
		if (pchk[step]) {
			stat = SYNTAX_EXTRA_PARAM;
			break;
		}
		/* assemble code if pass 2 */
		if (code->flag&FLAG_PASS_2) {
			pcod = codx_make_inst(code,bcod,CodeDB[pick].size,
				code->addr,code->line);
			codx_insert_inst(code,pcod);
			if (code->stat&STAT_OVERLAPPED_MEMORY)
				stat = SYNTAX_OVERLAPPED_MEMORY;
		}
		/* update address in any case */
		code->addr += CodeDB[pick].size;
	}
	while(0);
	return stat;
}
/*----------------------------------------------------------------------------*/
int codx_direct(my1code_t* code, char *parg, int pick) {
	volatile my1inst_t *tcod;
	my1inst_t *pcod;
	char *pchk;
	byte08_t *pbyte;
	int size, temp = 0, count = 0, stmp = 0, chk16 = 0, stat = SYNTAX_OK;

	tcod = (my1inst_t*)malloc(sizeof(my1inst_t));
	if (!tcod) return SYNTAX_MEMORY_ERROR;
	inst_init((my1inst_t*)tcod,0);
	/* false do-while loop - for error exits */
	do {
		/* should be exactly 1 argument */
		switch (TaskDB[pick].argtype) {
			case ARGTYPE_EQUA:
				/* expecting only ONE parameter */
				pchk = parg;
				do {
					stmp = get_strdata(pchk,(byte08_t*)&temp,0x0);
					if(stmp!=SYNTAX_NOT_STR)
						break;
					stmp = get_expression(pchk,(byte08_t*)&temp,code);
					if(stmp!=SYNTAX_NOT_EXPR)
						break;
					stmp = get_ivalue(pchk,(byte08_t*)&temp,1);
				} while(0);
				if (stmp==SYNTAX_OK) {
					if (code->csym) code->csym->data = temp;
					else stmp = SYNTAX_NO_LABEL;
				}
				break;
			case ARGTYPE_ADDR:
				/* expecting only ONE parameter */
				pchk = parg;
				do {
					stmp = get_strdata(pchk,(byte08_t*)&temp,0x0);
					if (stmp!=SYNTAX_NOT_STR) break;
					code->flag |= FLAG_MUST_RESOLVE;
					stmp = get_expression(pchk,(byte08_t*)&temp,code);
					code->flag &= ~FLAG_MUST_RESOLVE;
					if(stmp!=SYNTAX_NOT_EXPR) break;
					stmp = get_ivalue(pchk,(byte08_t*)&temp,1);
				} while(0);
				if (stmp==SYNTAX_OK)
					code->addr = temp;
				break;
			case ARGTYPE_ENDS:
				/* expecting only ONE parameter */
				pchk = parg;
				if (pchk) stmp = SYNTAX_EXTRA_PARAM;
				else code->flag |= FLAG_CODE_ENDS;
				break;
			case ARGTYPE_NOTI:
				/* unsupported directive e.g. 'cpu' - just ignore! */
				stmp = SYNTAX_OK;
				break;
			case ARGTYPE_EXTI:
				/* external directive - syntax ignored! */
				if (code->xtratask) {
					pcod = code->xtratask((void*)code,parg);
					if (pcod) codx_insert_inst(code,pcod);
				}
				stmp = SYNTAX_OK;
				break;
			case ARGTYPE_WORD:
				chk16 = 1; /* let this flow into next case! */
			case ARGTYPE_DATA:
				if (!parg) stmp = SYNTAX_NO_PARAM;
				else {
					/* start getting data vector */
					while (1) {
						/* look for strdata */
						pchk = check_delims(parg," ,\n\r",count);
						if (pchk) {
							/* check_delims already created a buffer */
							count++;
						}
						else break; /* no more? */
						/* main data extractor loop */
						do {
							/* check for str data if not chk16 */
							temp = 0; pbyte = 0x0;
							if (!chk16) {
								stmp = get_strdata(pchk,pbyte,(my1inst_t*)tcod);
								if (stmp!=SYNTAX_NOT_STR)
									break;
							}
							pbyte = (byte08_t*) &temp;
							stmp = get_expression(pchk,pbyte,code);
							if (stmp!=SYNTAX_NOT_EXPR) break;
							stmp = get_ivalue(pchk,pbyte,chk16);
						} while(0);
						/* free resources */
						free(pchk);
						/* nested loop */
						if (stmp<0) break;
						/* add data/word to codex */
						if (pbyte) {
							size = chk16 ? sizeof(word16_t) : sizeof(byte08_t);
							size += tcod->size;
							stmp = tcod->size;
							inst_resize((my1inst_t*)tcod,size);
							if (tcod->size!=size) {
								stmp = SYNTAX_MEMORY_ERROR;
								break;
							}
							tcod->data[stmp++] = pbyte[0];
							if (chk16)
								tcod->data[stmp] = pbyte[1];
							stmp = SYNTAX_OK;
						}
					};
				}
				if (stmp==SYNTAX_OK) {
					/* assemble data if no error and in pass 2 */
					if(code->flag&FLAG_PASS_2) {
						/** use line=0 to indicate NOT_AN_INSTRUCTION */
						pcod = codx_make_inst(code,tcod->data,
							tcod->size,code->addr,0);
						if (pcod) {
							codx_insert_inst(code,pcod);
							stmp = SYNTAX_OK;
						}
						else stmp = SYNTAX_MEMORY_ERROR;
					}
					code->addr += tcod->size;
				}
				break;
			case ARGTYPE_STOR:
				/* expecting only ONE parameter */
				pchk = parg;
				stmp = get_expression(pchk,(byte08_t*)&temp,code);
				if(stmp==SYNTAX_NOT_EXPR)
					stmp = get_ivalue(pchk,(byte08_t*)&temp,1);
				if(stmp==SYNTAX_OK)
					code->addr += temp;
				break;
			default:
				stmp = SYNTAX_CHECK_DIRECTIVE;
				break;
		}
		/* break for error */
		if (stmp<0) {
			stat = stmp;
			break;
		}
	} while (0);
	inst_free((my1inst_t*)tcod);
	free((void*)tcod);

	return stat;
}
/*----------------------------------------------------------------------------*/
int code85_parser(my1parser_t *parser) {
	my1code85_t *ptop;
	my1code_t *code;
	char *pcmd, *parg, *abuf;
	char *tbuf, *cbuf, *sbuf, *pchk;
	int loop, stat;
	/* get code object */
	ptop = (my1code85_t*)parser->encode;
	code = &ptop->code;
	/* increment line */
	code->line++;
	/* prepare stuff */
	parg = 0x0; abuf = 0x0;
	stat = SYNTAX_OK;
	/* reset current codex pointer */
	code->ccod = 0x0;
	/* reset current label pointer */
	if (code->dlab)
		code->csym = 0x0;
	/* skip whitespace to check 'empty' lines */
	sbuf = parser->rbuf;
	while (is_whitespace(*sbuf)) sbuf++;
	/* skip if empty lines! also ignore comments? */
	if (!sbuf[0]||sbuf[0]=='\n'||sbuf[0]=='\r'||sbuf[0]==';') {
#ifdef MY1DEBUG_CODE85
		fprintf(stderr,"LINE %3d: ", code->line);
		fprintf(stderr,"EmptyLine");
		fprintf(stderr,"?=%s",parser->rbuf);
		fprintf(stderr,"\n");
		fflush(stderr);
#endif
		return SYNTAX_OK;
	}
	/* create separate token processing buffer */
	tbuf = (char*) malloc(parser->fill+1);
	if (!tbuf) return SYNTAX_MEMORY_ERROR;
	strcpy(tbuf,sbuf);
	/* break down main line components */
	do {
		/* check if there is a comment */
		pchk = strchr(tbuf,';');
		if (pchk) pchk[0] = 0x0; /* end at comment! */
		/* check if there is a label */
		cbuf = strchr(tbuf,':');
		pchk = strchr(tbuf,'"');
		if (cbuf&&(!pchk||pchk>cbuf)) {
			pchk = strtok(tbuf,COM_DELIM);
			if (!pchk||pchk>cbuf) {
				stat = SYNTAX_INVALID_LABEL;
				break;
			}
			/* process in uppercase */
			str2upper(pchk);
			/* check whitespace in label */
			cbuf = pchk;
			while (*cbuf) {
				if (is_whitespace(*cbuf)) {
					stat = SYNTAX_INVALID_LABEL;
					break;
				}
				cbuf++;
			}
			if (stat<0) break;
			if (code->flag&FLAG_PASS_1) {
				/* check for existing label */
				if (codx_get_symbol(code,pchk)) {
					stat = SYNTAX_DUPLICATE_LABEL;
					break;
				}
				/* create and finalize label */
				if (!codx_make_symbol(code,pchk,code->addr,code->addr)) {
					stat = SYNTAX_MEMORY_ERROR;
					break;
				}
				code->csym->line = code->line;
			}
			else {
				/* refer to current label */
				code->csym = codx_get_symbol(code,pchk);
			}
			/* flag for label usage - prolonged lifetime */
			if(code->csym)
				code->dlab = 0;
			/* reset for next token */
			cbuf = 0x0;
		}
		else {
			/* keep the original pointer */
			cbuf = tbuf;
		}
		/* look for opcode mnemonic */
		pcmd = strtok(cbuf,OPC_DELIM);
		if (!pcmd) {
			/* maybe code is on the next line?? */
			break; /* made possible by false do-while loop! */
		}
		/* look for opcode arguments - ignore if cannot get one*/
		parg = strtok(0x0,END_DELIM);
		if (parg) {
			trimws(parg, 0x0, 0);
			/* only if contains non-whitespace */
			if (strlen(parg)) {
				/* create temp processing buffer */
				abuf = malloc(strlen(parg)+1);
				strcpy(abuf,parg);
			}
		}
		/* process in uppercase */
		str2upper(pcmd);
		/* encode the mnemonic or execute directive */
		/* browse opcode list */
		for (loop=0;loop<CODEDBSIZE;loop++) {
			if (!strcmp(pcmd,CodeDB[loop].instr)) {
				/* process in uppercase */
				if (abuf) str2upper(abuf);
				stat = codx_opcode(code,abuf,loop);
				break;
			}
		}
		/* found an opcode - skip directive check */
		if (loop<CODEDBSIZE) break;
		/* browse directive list if no opcode */
		for (loop=0;loop<TASKDBSIZE;loop++) {
			if (!strcmp(pcmd,TaskDB[loop].direc)) {
				/* process in uppercase - except data (could be string!) */
				if (abuf&&TaskDB[loop].argtype!=ARGTYPE_DATA)
					str2upper(abuf);
				stat = codx_direct(code,abuf,loop);
				if (code->flag&FLAG_CODE_ENDS)
					parser->flag |= PARSER_FLAG_END;
				break;
			}
		}
		if (loop<TASKDBSIZE) break;
		/* no match found! syntax error! */
		stat = SYNTAX_INVALID_OPCODE;
	} while(0);
	ptop->stat = stat;
	if (stat<0) {
		/* display line error */
		FILE* pout = (FILE*)ptop->show;
		if (pout) {
			if (!code->ecnt) fprintf(pout,"\n");
			fprintf(pout,"LINE %3d: ",code->line);
			switch (stat) {
				case SYNTAX_INVALID_OPCODE:
					fprintf(pout,"** Unknown opcode '%s'! (%s)\n",pcmd,parg);
					break;
				case SYNTAX_INVALID_PARAM:
					fprintf(pout,"** Invalid parameter for '%s'! (%s){%s}\n",
						pcmd,parg,parser->rbuf);
					break;
				case SYNTAX_NO_PARAM:
					fprintf(pout,"** No parameter given for '%s'? (%s)\n",
						pcmd,parg);
					break;
				case SYNTAX_EXTRA_PARAM:
					fprintf(pout,"** Extra parameter given for '%s'? (%s)\n",
						pcmd,parg);
					break;
				case SYNTAX_INVALID_LABEL:
					fprintf(pout,"** Invalid label! (%s)\n",sbuf);
					break;
				case SYNTAX_DUPLICATE_LABEL:
					fprintf(pout,"** Duplicate label '%s'! (%s)\n",pchk,sbuf);
					break;
				case SYNTAX_UNKNOWN_LABEL:
					fprintf(pout,"** Unknown label in '%s'!\n",sbuf);
					break;
				case SYNTAX_NO_LABEL:
					fprintf(pout,"** No label given for '%s'!\n",sbuf);
					break;
				case SYNTAX_CHECK_DIRECTIVE:
					fprintf(pout,"** Suspicious directive '%s'? (%s)\n",
						pcmd,sbuf);
					break;
				case SYNTAX_MEMORY_ERROR:
					fprintf(pout,"** Error extracting data for '%s'!\n",sbuf);
					break;
				case SYNTAX_OVERLAPPED_MEMORY:
					fprintf(pout,"** Overlapped address @ '%s'!\n",sbuf);
					break;
				case SYNTAX_INVALID_OPS:
					fprintf(pout,"** Invalid operator@reference '%s'!\n",parg);
					break;
				case SYNTAX_MUST_RESOLVE_ADDR:
					fprintf(pout,"** Unresolved label! (%s)\n",sbuf);
					break;
				default:
					fprintf(pout,"** Unknown error! (%s){%s}[%d]\n",
						sbuf,parser->rbuf,stat);
					break;
			}
			fflush(pout);
		}
		if (ptop->echk) ptop->echk((void*)ptop);
		code->ecnt++;
	}
	else {
		/* assume label used if opcode/directive found (no error!) */
		code->dlab = 1; /* reset/delete label for next line */
#ifdef MY1DEBUG_CODE85
		fprintf(stderr,"LINE %3d: Label='%s': Mnemonic='%s' - Argument='%s'\n",
			code->line,code->csym?code->csym->name:"(none)",
			pcmd?pcmd:"(none)",parg?parg:"(none)");
		fflush(stderr);
#endif
		if (ptop->ccpy&&(code->lcod&&code->lcod->line==code->line)) {
			ptop->pcmd = pcmd;
			ptop->parg = parg;
			ptop->ccpy(ptop);
		}
	}
	/* free the processing buffer */
	free((void*)tbuf);
	if (abuf) free((void*)abuf);
	return stat;
}
/*----------------------------------------------------------------------------*/
void code85_init(my1code85_t* that) {
	codx_init(&that->code);
	parser_init(&that->read);
	that->read.encode = (void*)that;
	that->read.doread = code85_parser;
	that->ccpy = 0x0;
	that->echk = 0x0;
	that->eobj = 0x0;
	/* that->temp = 0x0; */
	that->show = (void*)stderr;
	that->flag = 0;
	that->stat = 0;
}
/*----------------------------------------------------------------------------*/
void code85_free(my1code85_t* that) {
	parser_free(&that->read);
	codx_free(&that->code);
}
/*----------------------------------------------------------------------------*/
int code85_read(my1code85_t* that, char* name) {
	int pass, show;
	show = 0;
	pass = that->code.flag&FLAG_PASS_MASK;
	if (!pass) {
		codx_prep(&that->code,FLAG_PASS_1);
		pass = FLAG_PASS_1;
		show++;
	}
	else if (pass==FLAG_PASS_1) {
		codx_prep(&that->code,FLAG_PASS_2);
		pass = FLAG_PASS_2;
		show++;
	}
	else if (pass==FLAG_PASS_2) {
		if (that->show) {
			FILE* pout = (FILE*)that->show;
			fprintf(pout,"** Done?\n");
			fflush(pout);
		}
		pass = FLAG_PASS_MASK;
	}
	else {
		if (that->show) {
			FILE* pout = (FILE*)that->show;
			fprintf(pout,"** Invalid pass! (%d)\n",pass);
			fflush(pout);
		}
	}
	if (show) {
		if (!(that->flag&CODE85_FLAG_QUIET)) {
			printf("PASS %d: ",pass);
			fflush(stdout);
		}
		parser_read(&that->read,name);
		if (!(that->flag&CODE85_FLAG_QUIET)) {
			printf("Lines = %d, Error = %d\n",that->code.line,that->code.ecnt);
			fflush(stdout);
		}
	}
	return that->read.ecnt;
}
/*----------------------------------------------------------------------------*/
