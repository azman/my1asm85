/*----------------------------------------------------------------------------*/
#ifndef __MY1INST2VMF_H__
#define __MY1INST2VMF_H__
/*----------------------------------------------------------------------------*/
#include "my1instruction.h"
/*----------------------------------------------------------------------------*/
#define VMF_ERROR_FILE -1
#define VMF_ERROR_ADDR -2
/*----------------------------------------------------------------------------*/
#include <stdio.h>
/*----------------------------------------------------------------------------*/
int inst_2vmf(my1inst_t *pcod, char *file) {
	FILE *pvmf;
	my1inst_t *tcod;
	int loop, ecnt, addr;
	/* initialize */
	ecnt = 0; addr = 0x0000;
	/* open file */
	pvmf = fopen(file,"w");
	if (!pvmf) return VMF_ERROR_FILE;
	/* browse through code */
	tcod = pcod;
	/* initialize first address */
	if (tcod) {
		addr = tcod->addr;
		if (!addr)
			fprintf(pvmf,"@%04x\n",addr);
	}
	while (tcod) {
		if (tcod->line>=0) {
			if (tcod->addr<addr) {
				ecnt = VMF_ERROR_ADDR;
				break;
			}
			else if (tcod->addr>addr) {
				addr = tcod->addr;
				fprintf(pvmf,"@%04x\n",addr);
			}
			for (loop=0;loop<tcod->size;loop++) {
				fprintf(pvmf,"%02x\n",tcod->data[loop]);
				addr++;
			}
		}
		tcod = tcod->next;
	}
	fclose(pvmf);
	return ecnt;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1INST2VMF_H__ */
/*----------------------------------------------------------------------------*/
