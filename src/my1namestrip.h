/*----------------------------------------------------------------------------*/
#ifndef __MY1NAMESTRIP_H__
#define __MY1NAMESTRIP_H__
/*----------------------------------------------------------------------------*/
#ifdef DO_MINGW
#define PATH_SEP '\\'
#else
#define PATH_SEP '/'
#endif
/*----------------------------------------------------------------------------*/
char* stripfilename(char *name, int noext, int nopath) {
	int init, idot, step, loop, size;
	init = 0; idot = -1; step = 0;
	while (name[step]) step++;
	for (loop=step-1;loop>=0;loop--) {
		if (name[loop]=='.') {
			if (idot<0) idot = loop;
		}
		if (nopath&&name[loop]==PATH_SEP) {
			init = loop + 1;
			break;
		}
	}
	size = step - init;
	if (noext&&idot>=0)
		size -= step - idot;
	if (init>=0) {
		for (loop=0;loop<size;loop++)
			name[loop] = name[loop+init];
		name[loop] = 0x0;
	}
	return name;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1NAMESTRIP_H__ */
/*----------------------------------------------------------------------------*/
