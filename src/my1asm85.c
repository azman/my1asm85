/*----------------------------------------------------------------------------*/
#include "my1code85.h"
#include "my1namestrip.h"
#include "my1code2lst.h"
#include "my1inst2hex.h"
#include "my1inst2vmf.h"
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
/*----------------------------------------------------------------------------*/
#define PROGNAME "my1asm85"
#ifndef PROGVERS
#define PROGVERS "build"
#endif
/*----------------------------------------------------------------------------*/
#define STAT_ERROR_ARGS 0x0001
#define STAT_ERROR_MALLOC 0x0002
/*----------------------------------------------------------------------------*/
#define FLAG_MAKE_HEX 0x0001
#define FLAG_MAKE_LST 0x0002
#define FLAG_MAKE_VMF 0x0004
#define FLAG_MAKE_MASK (FLAG_MAKE_HEX|FLAG_MAKE_LST|FLAG_MAKE_VMF)
/*----------------------------------------------------------------------------*/
void usage(void) {
	printf("Use: %s [options] [filename]\n\n",PROGNAME);
	printf("Options are:\n");
	printf("  --hex  : generate HEX file\n");
	printf("  --list : generate LST file\n");
	printf("  --vmf  : generate Verilog memory file\n");
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	my1code85_t code85;
	FILE *plst;
	char *fcod, *name;
	unsigned stat, flag;
	int loop, size, test;
	/* print tool info */
	printf("\n%s - 8085 Cross Assembler (version-%s)\n",PROGNAME,PROGVERS);
	printf("  => by azman@my1matrix.org\n\n");
	code85_init(&code85);
	plst = 0x0; fcod = 0x0; name = 0x0; stat = 0; flag = 0;
	do {
		for (loop=1;loop<argc;loop++) {
			if (strcmp(argv[loop],"--hex")==0)
				flag |= FLAG_MAKE_HEX;
			else if (strcmp(argv[loop],"--list")==0)
				flag |= FLAG_MAKE_LST;
			else if (strcmp(argv[loop],"--vmf")==0)
				flag |= FLAG_MAKE_VMF;
			else if (strcmp(argv[loop],"--quiet")==0) {
				code85_doquiet(&code85);
				code85_show(&code85,0x0);
			}
			else if (argv[loop][0]=='-') {
				fprintf(stderr,"** Unknown parameter %s\n",argv[loop]);
				stat |=  STAT_ERROR_ARGS;
			}
			else {
				if (fcod) {
					fprintf(stderr,"** Multiple input '%s' & '%s'\n",
						argv[loop],fcod);
					stat |=  STAT_ERROR_ARGS;
				}
				else fcod = argv[loop];
			}
		}
		if (stat) break;
		if (argc>1) {
			if (!fcod) {
				fprintf(stderr, "** No input file given!\n");
				break;
			}
		}
		else {
			usage();
			break;
		}
		if (flag&FLAG_MAKE_MASK) {
			size = strlen(fcod)+5;
			name = malloc(size);
			if (!name) {
				fprintf(stderr, "** Cannot allocate memory for filename!\n");
				stat |= STAT_ERROR_MALLOC;
				break;
			}
			strcpy(name,fcod);
			stripfilename(name,1,1);
			size = strlen(name);
		}
		/* this is a go! */
		if (code85_read(&code85,fcod))
			break;
		if (flag&FLAG_MAKE_LST) {
			name[size] = 0x0; /* just in case */
			strcat(name,".lst");
			plst = fopen(name,"w");
			if (!plst)
				fprintf(stderr, "** Cannot open file for listing!\n");
			else {
				code85.temp = (void*) plst;
				code85.read.doxtra = code85_lister;
			}
		}
		code85_read(&code85,fcod);
		if (plst) {
			/* show labels */
			list_write(plst,&code85.code,0x0);
			fclose(plst);
			printf("\n-- LST file (%s) generated!\n",name);
			fflush(stdout);
		}
		if (code85.read.ecnt)
			break;
		/* check request for output file(s) */
		if (flag&FLAG_MAKE_HEX) {
			name[size] = 0x0; /* just in case */
			strcat(name,".hex");
			printf("\n-- Generating HEX file (%s)... ",name);
			fflush(stdout);
			test = inst_2hex(code85.code.pcod,name);
			printf("%s (%d)\n",test?"error?":"done.",test);
		}
		if (flag&FLAG_MAKE_VMF) {
			name[size] = 0x0; /* just in case */
			strcat(name,".vmf");
			printf("\n-- Generating VMF file (%s)... ",name);
			fflush(stdout);
			test = inst_2vmf(code85.code.pcod,name);
			printf("%s (%d)\n",test?"error?":"done.",test);
		}
	} while (0);
	if (name) free((void*)name);
	/* show summary */
	if (!stat&&code85.read.line>0) {
		printf("\n%s - Done!",PROGNAME);
		if (!(flag&FLAG_MAKE_MASK)) {
			printf(" [%d:",code85.read.line);
			if (code85.read.ecnt)
				printf("Found %d error(s)",code85.read.ecnt);
			else printf("OK");
			printf("]");
		}
		printf("\n");
		stat = code85.read.ecnt;
	}
	printf("\n");
	code85_free(&code85);
	return stat;
}
/*----------------------------------------------------------------------------*/
