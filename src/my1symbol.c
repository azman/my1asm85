/*----------------------------------------------------------------------------*/
#include "my1symbol.h"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
void symb_init(my1symb_t* psym, char* text, int addr, int data) {
	int size;
	size = strlen(text)+1;
	psym->name = (char*) malloc(size);
	if (psym->name)
		strncpy(psym->name,text,size);
	psym->addr = addr;
	psym->data = data;
	psym->line = 0;
	psym->next = 0x0;
}
/*----------------------------------------------------------------------------*/
void symb_free(my1symb_t* psym) {
	if (psym->name) {
		free((void*)(psym->name));
		psym->name = 0x0;
	}
}
/*----------------------------------------------------------------------------*/
