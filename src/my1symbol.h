/*----------------------------------------------------------------------------*/
#ifndef __MY1SYMBOL_H__
#define __MY1SYMBOL_H__
/*----------------------------------------------------------------------------*/
typedef struct _my1symb_t {
	char *name; /* symbol name */
	int addr, data; /* can represent addr OR data (EQU directive) */
	int line;
	struct _my1symb_t *next;
} my1symb_t;
/*----------------------------------------------------------------------------*/
void symb_init(my1symb_t* psym, char* text, int addr, int data);
void symb_free(my1symb_t* psym);
/*----------------------------------------------------------------------------*/
#endif /** __MY1SYMBOL_H__ */
/*----------------------------------------------------------------------------*/
