/*----------------------------------------------------------------------------*/
#include "my1instruction.h"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
void inst_init(my1inst_t* pcod, int size) {
	pcod->data = 0x0;
	pcod->size = 0;
	pcod->addr = 0;
	pcod->line = 0;
	pcod->lbsz = 0;
	pcod->lbuf = 0x0;
	pcod->next = 0x0;
	inst_resize(pcod,size);
}
/*----------------------------------------------------------------------------*/
void inst_free(my1inst_t* pcod) {
	if (pcod->data) {
		free((void*)pcod->data);
		pcod->data = 0x0;
		pcod->size = 0;
	}
	if (pcod->lbuf) {
		free((void*)pcod->lbuf);
		pcod->lbsz = 0;
	}
}
/*----------------------------------------------------------------------------*/
void inst_resize(my1inst_t* pcod, int size) {
	byte08_t* test;
	if (size==pcod->size) return;
	if (size>0) {
		test = (byte08_t*) realloc((void*)pcod->data,sizeof(byte08_t)*size);
		if (test) {
			pcod->data = test;
			pcod->size = size;
		}
	}
	else {
		if (pcod->data) {
			free((void*)pcod->data);
			pcod->data = 0x0;
			pcod->size = 0;
		}
	}
}
/*----------------------------------------------------------------------------*/
my1inst_t* inst_clone(my1inst_t* pcod) {
	my1inst_t* pnew;
	pnew = (my1inst_t*) malloc(sizeof(my1inst_t));
	if (pnew) {
		inst_init(pnew,pcod->size);
		if (pnew->data) {
			memcpy(pnew->data,pcod->data,pcod->size);
			pnew->addr = pcod->addr;
			pnew->line = pcod->line;
		}
		if (pcod->lbuf) {
			pnew->lbsz = strlen(pcod->lbuf)+1;
			pnew->lbuf = (char*)malloc(pnew->lbsz);
			if (pnew->lbuf) strcpy(pnew->lbuf,pcod->lbuf);
			else pnew->lbsz = 0;
		}
		pnew->next = 0x0;
	}
	return pnew;
}
/*----------------------------------------------------------------------------*/
int inst_compare(my1inst_t *pcod, my1inst_t *qcod) {
	int flag = 0;
	if ((pcod->addr)>=(qcod->addr+qcod->size)) flag = -1;
	else if ((qcod->addr)>=(pcod->addr+pcod->size)) flag = 1;
	return flag;
}
/*----------------------------------------------------------------------------*/
