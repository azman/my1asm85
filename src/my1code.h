/*----------------------------------------------------------------------------*/
#ifndef __MY1CODE_H__
#define __MY1CODE_H__
/*----------------------------------------------------------------------------*/
#include "my1symbol.h"
#include "my1instruction.h"
/*----------------------------------------------------------------------------*/
#define FLAG_MUST_RESOLVE 0x0100
#define FLAG_CODE_ENDS 0x8000
/*----------------------------------------------------------------------------*/
#define FLAG_PASS_1 0x0001
#define FLAG_PASS_2 0x0002
#define FLAG_PASS_MASK (FLAG_PASS_1|FLAG_PASS_2)
/*----------------------------------------------------------------------------*/
#define STAT_OVERLAPPED_MEMORY 0x0001
/*----------------------------------------------------------------------------*/
typedef struct _my1code_t {
	/* internal stat */
	unsigned int stat, flag;
	int addr, line; /* need to be signed */
	int ecnt, dlab;
	/* symbol table - linked-list */
	my1symb_t *psym, *lsym, *csym;
	/* code sequence - linked-list */
	my1inst_t *pcod, *lcod, *ccod;
	/* checking user-defined directive */
	void* (*xtratask)(void*,char*);
} my1code_t;
/*----------------------------------------------------------------------------*/
void codx_init(my1code_t* code);
void codx_free(my1code_t* code);
void codx_prep(my1code_t* code, int pass);
my1symb_t* codx_get_symbol(my1code_t* code, char *parg);
int codx_find_symbol(my1code_t* code, byte08_t *byte, char *parg);
my1symb_t* codx_make_symbol(my1code_t* code, char* text, int addr, int data);
my1inst_t* codx_make_inst(my1code_t* code, byte08_t* bcod,
		int size, int addr, int line);
void codx_insert_inst(my1code_t* code, my1inst_t* pcod);
/*----------------------------------------------------------------------------*/
#endif /** __MY1CODE_H__ */
/*----------------------------------------------------------------------------*/
