/*----------------------------------------------------------------------------*/
#ifndef __MY1STRCHK_H__
#define __MY1STRCHK_H__
/*----------------------------------------------------------------------------*/
#define OPC_DELIM " \t\n\r"
#define COM_DELIM ":"
#define END_DELIM "\n\r"
#define ARG_DELIM ",\n\r"
#define TOK_DELIM " \t"
/*----------------------------------------------------------------------------*/
/** list is delims */
#define is_delim is_listed
/** list is EXPR_OPRS */
#define is_operator is_listed
/*----------------------------------------------------------------------------*/
char is_whitespace(char that);
char is_listed(char that, char *list);
char is_alpha(char that);
char is_label_init(char achar);
char uppercase(char *that);
char* str2upper(char *pstr);
char* trimws(char *pstr, int *trim, int norepeat);
char* get_operator(char *pstr, char *list, int *size);
/* these functions return malloc'ed buffer (if not null) */
char* check_quotes(char *pstr, int size);
char* check_delims(char *pstr, char *list, int step);
/*----------------------------------------------------------------------------*/
#endif /** __MY1STRCHK_H__ */
/*----------------------------------------------------------------------------*/
