/*----------------------------------------------------------------------------*/
#include "my1parser.h"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
/*----------------------------------------------------------------------------*/
void parser_init(my1parser_t* that) {
	that->size = MAX_LINESIZE;
	that->rbuf = (char*) malloc(that->size);
	that->fill = 0;
	that->line = 0;
	that->ecnt = 0;
	that->stat = 0;
	that->flag = 0;
	that->encode = 0x0;
	that->doread = 0x0;
	that->doxtra = 0x0;
}
/*----------------------------------------------------------------------------*/
void parser_free(my1parser_t* that) {
	free((void*)that->rbuf);
}
/*----------------------------------------------------------------------------*/
int parser_readline(my1parser_t* that, FILE *file) {
	char *temp, buff[MAX_LINESIZE];
	int size, test, full, done;
	test = 0;
	done = 0; /* in case of long lines */
	that->rbuf[0] = 0x0;
	while (!done) {
		/* read a line, limited buffer */
		if (!fgets((char*)&buff,MAX_LINESIZE,file))
			return -1;
		/* got a line - count & check for newline */
		size = 0;
		while (buff[size]) {
			if (buff[size]=='\n'||buff[size]=='\r') {
				buff[size] = 0x0;
				done = 1;
				break;
			}
			size++;
		}
		/* accmulate into parser buff */
		if (size>0) {
			full = size + test + 1; /* plus null */
			if (full>that->size) {
				/* reallocs buffer as needed */
				temp = realloc((void*)that->rbuf,full);
				if (temp) {
					that->rbuf = temp;
					that->size = full;
				} else size = that->size-test-1; /* rsvp null */
			}
			strcat(that->rbuf,buff);
			test += size; /* return value is char count only! */
			that->fill = test;
			if (feof(file)) done = 1; /* done if EOF */
		}
	}
	return test;
}
/*----------------------------------------------------------------------------*/
int parser_read(my1parser_t* that, char* name) {
	FILE *from;
	that->line = 0;
	that->ecnt = 0;
	from = fopen(name,"r");
	if (!from) {
		that->stat |= PARSER_READ_ERROR;
		that->ecnt++;
	}
	else {
		that->flag &= ~PARSER_FLAG_END; /* just in case */
		while (parser_readline(that,from)!=-1) {
			that->line++;
#ifdef MY1DEBUG_PARSER
			printf("@@ {DEBUG:%s}\n",that->rbuf);
#endif
			/* parse the line! */
			if (that->doread) {
				if (that->doread(that))
					that->ecnt++;
			}
			if (that->doxtra)
				that->doxtra(that);
			/* check for user-requested end */
			if (that->flag&PARSER_FLAG_END) break;
		}
		fclose(from);
		if (that->ecnt) that->stat |= PARSER_SYNTAX_ERROR;
	}
	return that->ecnt;
}
/*----------------------------------------------------------------------------*/
