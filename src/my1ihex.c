/*----------------------------------------------------------------------------*/
#include "my1ihex.h"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <stdio.h>
/*----------------------------------------------------------------------------*/
my1ihex_data_t* ihex_data_make(int addr) {
	my1ihex_data_t* make;
	if (addr<0||addr>0xffff) return 0x0; /* validate 16-bit address */
	make = (my1ihex_data_t*)malloc(sizeof(my1ihex_data_t));
	if (make) {
		make->addr = addr;
		make->fill = 0;
	}
	return make;
}
/*----------------------------------------------------------------------------*/
int ihex_data_push(my1ihex_data_t* hdat, hexd_t* data, int size) {
	int loop;
	/** if (size>(IHEX_DATA_SIZE-fill)) return 0; >> asserted! **/
	for (loop=0;loop<size;loop++)
		hdat->data[hdat->fill++] = data[loop];
	return size;
}
/*----------------------------------------------------------------------------*/
/* creates malloc'ed char array */
char* ihex_data_line(my1ihex_data_t* hdat) {
	char *line, *temp;
	int size, csum, loop;
	hexd_t head[HEX_DATA_OFFS];
	if (!hdat->fill) return 0x0;
	line = 0x0;
	size = 2+((HEX_DATA_OFFS+hdat->fill+1)<<1); /* 2-char per byte */
	line = (char*)malloc(sizeof(char)*size);
	if (line) {
		head[HEX_INDEX_CNT] = hdat->fill&0xff;
		head[HEX_INDEX_ADH] = (hdat->addr>>8)&0xff;
		head[HEX_INDEX_ADL] = hdat->addr&0xff;
		head[HEX_INDEX_TYP] = 00; /* data type */
		sprintf(line,"%c%02X%02X%02X%02X",HEX_INIT_CHAR,
			head[0],head[1],head[2],head[3]);
		/* calculate checksum */
		for (loop=0,csum=0;loop<HEX_DATA_OFFS;loop++)
			csum += head[loop];
		/* convert as well */
		temp = &line[IHEX_LINE_OFFS];
		for (loop=0;loop<hdat->fill;loop++) {
			csum += hdat->data[loop];
			sprintf(temp,"%02X",hdat->data[loop]);
			temp += 2;
		}
		csum = ~csum + 1;
		sprintf(temp,"%02X",(csum&0xff));
	}
	return line;
}
/*----------------------------------------------------------------------------*/
void ihex_init(my1ihex_t* ihex) {
	ihex->stat = 0;
	ihex->rows = 0;
	ihex->init = 0;
}
/*----------------------------------------------------------------------------*/
void ihex_free(my1ihex_t* ihex) {
	int loop;
	if (ihex->init) {
		for (loop=0;loop<ihex->rows;loop++)
			free((void*)ihex->init[loop]);
		free((void*)ihex->init);
	}
}
/*----------------------------------------------------------------------------*/
int ihex_more(my1ihex_t* ihex, int addr) {
	my1ihex_data_t** temp;
	my1ihex_data_t* make;
	int size;
	make = ihex_data_make(addr);
	if (!make) return 0;
	size = ihex->rows+1;
	temp = (my1ihex_data_t**)realloc((void*)ihex->init,
		sizeof(my1ihex_data_t*)*size);
	if (temp) {
		ihex->init = temp;
		ihex->init[ihex->rows] = make;
		ihex->rows = size;
	}
	else {
		free((void*)make);
		make = 0x0;
	}
	return make?1:0;
}
/*----------------------------------------------------------------------------*/
int ihex_push(my1ihex_t* ihex, hexd_t* data, int size, int addr) {
	my1ihex_data_t *curr;
	int rems, step;
	curr = ihex->rows>0 ? (ihex->init[ihex->rows-1]) : 0x0;
	/* validate address */
	if (curr&&ihex_data_curr(curr)>addr) {
		/* should be incremental */
		return 0;
	}
	if (!curr||!ihex_data_slot(curr)||ihex_data_curr(curr)<addr) {
		/* create new */
		if (!ihex_more(ihex,addr)) return 0;
		curr = ihex_last(ihex);
	}
	rems = size;
	step = ihex_data_slot(curr);
	while (rems>step) {
		ihex_data_push(curr,data,step);
		rems -= step;
		addr += step;
		data += step;
		if (!ihex_more(ihex,addr)) return 0;
		curr = ihex_last(ihex);
		step = ihex_data_slot(curr);
	}
	if (rems) ihex_data_push(curr,data,rems);
	return size; /* assume ok if gets here */
}
/*----------------------------------------------------------------------------*/
int ihex_read_hexascii(char pstr) {
	int ival;
	if (pstr>=0x61) ival = (pstr-0x61)+10;
	else if (pstr>=0x41) ival = (pstr-0x41)+10;
	else if (pstr>=0x30) ival = (pstr-0x30);
	return ival;
}
/*----------------------------------------------------------------------------*/
int ihex_read_byte(char* pstr) {
	return (ihex_read_hexascii(pstr[0])<<4)+ihex_read_hexascii(pstr[1]);
}
/*----------------------------------------------------------------------------*/
int ihex_write(my1ihex_t* ihex, char* name) {
	FILE* file;
	char *temp;
	int stat, loop;
	if (name) {
		file = fopen(name,"w");
		if (!file) return -1;
	}
	else file = stdout;
	stat = 0;
	for (loop=0;loop<ihex->rows;loop++) {
		temp = ihex_data_line(ihex->init[loop]);
		if (temp) {
			fprintf(file,"%s\n",temp);
			fflush(file);
			free((void*)temp);
		}
		else stat--;
	}
	/* send EOF record */
	fprintf(file,":00000001FF\n");
	if (name) fclose(file);
	return 0;
}
/*----------------------------------------------------------------------------*/
int ihex_read(my1ihex_t* ihex, char* name) {
	my1ihex_data_t *curr;
	FILE* file;
	char buff[IHEX_LINE_SIZE];
	int stat, size, temp, addr, loop, step;
	unsigned int csum;
	file = fopen(name,"r");
	if (!file) return -1;
	stat = 0;
	while (!feof(file)) {
		/* read a line, limited buffer */
		if (!fgets((char*)&buff,IHEX_LINE_SIZE,file))
			break;
		/* got a line - count & check for newline */
		size = 0;
		while (buff[size]) {
			if (buff[size]=='\r'||buff[size]=='\n') {
				buff[size] = 0x0;
				break;
			}
			else if (!(buff[size]>=0x30&&buff[size]<=0x39)&&
					!(buff[size]>=0x41&&buff[size]<=0x46)&&
					!(buff[size]>=0x61&&buff[size]<=0x66)) {
				if (size!=0||buff[size]!=HEX_INIT_CHAR)
					break;
			}
			size++;
		}
		/* validate */
		if (buff[size]||size<IHEX_LINE_CMIN||((size-1)%2)) {
			stat--;
			continue;
		}
		/* get addr (high byte) */
		addr = ihex_read_byte(&buff[3]);
		csum = addr;
		addr <<= 8;
		/* get addr (low byte) */
		temp = ihex_read_byte(&buff[5]);
		csum += temp;
		addr += temp;
		/* get data size */
		temp = ihex_read_byte(&buff[1]);
		csum += temp;
		if ((temp+HEX_DATA_CORE)!=((size-1)/2)) {
			fprintf(stderr,"** Wrong size! (%d|%d)[%s]\n",
				(temp+HEX_DATA_CORE),((size-1)/2),buff);
			stat--;
			continue;
		}
		/* check type */
		step = ihex_read_byte(&buff[7]);
		if (step) /* skip if not data */
			continue;
		/* create new */
		if (!ihex_more(ihex,addr)) {
			fprintf(stderr,"** Cannot create data! (%04X)\n",addr);
			stat--;
			continue;
		}
		curr = ihex_last(ihex);
		for (loop=0,step=IHEX_LINE_OFFS;loop<temp;loop++,step+=2) {
			curr->data[loop] = ihex_read_byte(&buff[step]);
			curr->fill++;
			csum += curr->data[loop];
		}
		csum = (~csum+1)&0xff;
		/* get checksum */
		temp = ihex_read_byte(&buff[step]);
		if (csum!=temp) {
			fprintf(stderr,"** Wrong checksum! (%02X|%02X)[%s]\n",
				csum,temp,buff);
			stat--;
		}
	}
	fclose(file);
	return stat;
}
/*----------------------------------------------------------------------------*/
