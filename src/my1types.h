/*----------------------------------------------------------------------------*/
#ifndef __MY1TYPES_H__
#define __MY1TYPES_H__
/*----------------------------------------------------------------------------*/
#ifndef UINT32_MSB1
#define UINT32_MSB1 (~(~0U>>1))
#endif
#ifndef STAT_OK
#define STAT_OK 0
#endif
#ifndef STAT_ERROR
#define STAT_ERROR UINT32_MSB1
#endif
/*----------------------------------------------------------------------------*/
typedef unsigned char byte08_t;
typedef unsigned short word16_t;
typedef unsigned int word32_t;
/*----------------------------------------------------------------------------*/
#endif /** __MY1TYPES_H__ */
/*----------------------------------------------------------------------------*/
