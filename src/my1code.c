/*----------------------------------------------------------------------------*/
#include "my1code.h"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
void codx_init(my1code_t* code) {
	code->stat = 0;
	code->flag = 0;
	code->addr = 0;
	code->line = 0;
	code->ecnt = 0;
	code->dlab = 0;
	code->psym = 0x0; /* init symbol in link list */
	code->lsym = 0x0; /* last symbol in link list */
	code->csym = 0x0;
	code->pcod = 0x0; /* init codex */
	code->lcod = 0x0; /* last codex */
	code->ccod = 0x0;
	code->xtratask = 0x0;
}
/*----------------------------------------------------------------------------*/
void codx_free(my1code_t* code) {
	my1symb_t *psym;
	my1inst_t *pcod;
	/* clear symbols */
	while (code->psym) {
		psym = code->psym;
		code->psym = code->psym->next;
		symb_free(psym);
		free((void*)psym);
	}
	/* clear codes */
	while (code->pcod) {
		pcod = code->pcod;
		code->pcod = code->pcod->next;
		inst_free(pcod);
		free((void*)pcod);
	}
}
/*----------------------------------------------------------------------------*/
void codx_prep(my1code_t* code, int pass) {
	code->flag &= ~FLAG_PASS_MASK;
	code->flag |= pass;
	code->flag &= ~FLAG_CODE_ENDS;
	code->addr = 0;
	code->line = 0;
}
/*----------------------------------------------------------------------------*/
my1symb_t* codx_get_symbol(my1code_t* code, char *parg) {
	my1symb_t* psym = code->psym;
	/* browse symbols */
	while (psym) {
		if (!strcmp(psym->name,parg))
			break;
		psym = psym->next;
	}
	return psym;
}
/*----------------------------------------------------------------------------*/
int codx_find_symbol(my1code_t* code, byte08_t *byte, char *parg) {
	my1symb_t *psym;
	int *full, test;
	full = (int*) byte;
	test = -1;
	psym = codx_get_symbol(code, parg);
	if (psym) {
		test = 0;
		if (full)
			*full = psym->data;
	}
	return test;
}
/*----------------------------------------------------------------------------*/
my1symb_t* codx_make_symbol(my1code_t* code, char* text, int addr, int data) {
	my1symb_t* psym;
	psym = (my1symb_t*)malloc(sizeof(my1symb_t));
	if (psym) {
		symb_init(psym,text,addr,data);
		code->csym = psym;
		if (code->lsym)
			code->lsym->next = code->csym;
		else
			code->psym = code->csym;
		code->lsym = code->csym;
	}
	return psym;
}
/*----------------------------------------------------------------------------*/
my1inst_t* codx_make_inst(my1code_t* code, byte08_t* bcod,
		int size, int addr, int line) {
	my1inst_t *pcod;
	int loop;
	pcod = (my1inst_t*)malloc(sizeof(my1inst_t));
	if (pcod) {
		inst_init(pcod,size);
		if (pcod->data) {
			for (loop=0;loop<pcod->size;loop++)
				pcod->data[loop] = bcod[loop];
			pcod->addr = addr;
			pcod->line = line;
		}
	}
	return pcod;
}
/*----------------------------------------------------------------------------*/
void codx_insert_inst(my1code_t* code, my1inst_t* pcod) {
	my1inst_t *tcod, *vcod;
	if (code->lcod) {
		/* sort based on addr... usually sequential */
		if (pcod->addr>code->lcod->addr) {
			/* check overlapped address */
			if (!inst_compare(pcod,code->lcod))
				code->stat |= STAT_OVERLAPPED_MEMORY;
			/* append to list */
			code->lcod->next = pcod;
			code->lcod = pcod;
		}
		else {
			vcod = 0x0;
			tcod = code->pcod;
			/* logically should never get to the last item */
			while (tcod->addr<pcod->addr) {
				vcod = tcod;
				tcod = tcod->next;
			}
			/* check overlapped address? */
			if (!inst_compare(pcod,tcod))
				code->stat |= STAT_OVERLAPPED_MEMORY;
			else if (vcod&&!inst_compare(pcod,vcod))
				code->stat |= STAT_OVERLAPPED_MEMORY;
			/* check if first item */
			if(!vcod) code->pcod = pcod;
			else vcod->next = pcod;
			pcod->next = tcod;
		}
	}
	else {
		/* empty list... simply add in! */
		code->pcod = pcod;
		code->lcod = pcod;
	}
	code->ccod = pcod;
}
/*----------------------------------------------------------------------------*/
