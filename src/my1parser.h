/*----------------------------------------------------------------------------*/
#ifndef __MY1PARSER_H__
#define __MY1PARSER_H__
/*----------------------------------------------------------------------------*/
#define MAX_LINESIZE 256
/*----------------------------------------------------------------------------*/
#include "my1types.h"
/*----------------------------------------------------------------------------*/
#define PARSER_ERROR (STAT_ERROR|0x0001)
#define PARSER_READ_ERROR (STAT_ERROR|0x0002)
#define PARSER_SYNTAX_ERROR (STAT_ERROR|0x0004)
/*----------------------------------------------------------------------------*/
#define PARSER_FLAG_END 0x8000
/*----------------------------------------------------------------------------*/
/** forward declaration */
struct _my1parser_t;
/*----------------------------------------------------------------------------*/
typedef int (*line_parser_t)(struct _my1parser_t *parser);
/*----------------------------------------------------------------------------*/
typedef struct _my1parser_t {
	char *rbuf;
	int size, fill;
	int line, ecnt;
	unsigned int stat, flag;
	void *encode;
	line_parser_t doread;
	line_parser_t doxtra;
} my1parser_t;
/*----------------------------------------------------------------------------*/
void parser_init(my1parser_t* that);
void parser_free(my1parser_t* that);
int parser_read(my1parser_t* that, char* name);
/*----------------------------------------------------------------------------*/
#endif /** __MY1PARSER_H__ */
/*----------------------------------------------------------------------------*/
