/*----------------------------------------------------------------------------*/
#ifndef __MY1IHEX_H__
#define __MY1IHEX_H__
/*----------------------------------------------------------------------------*/
/* intel hex-80 format */
#define HEX_INIT_CHAR ':'
#define HEX_INDEX_CNT 0
#define HEX_INDEX_ADH 1
#define HEX_INDEX_ADL 2
#define HEX_INDEX_TYP 3
#define HEX_DATA_OFFS (1+2+1)
#define MAX_IHEX_SIZE 0x20
#define USE_IHEX_SIZE 0x10
/* +1 checksum byte */
#define HEX_DATA_CORE (HEX_DATA_OFFS+1)
#define MAX_IHEX_DATA (HEX_DATA_CORE+MAX_IHEX_SIZE)
#define USE_IHEX_DATA (HEX_DATA_CORE+USE_IHEX_SIZE)
/* +4 (init,\r,\n,\0) */
#define MAX_IHEX_LINE ((MAX_IHEX_DATA<<1)+4)
#define USE_IHEX_LINE ((USE_IHEX_DATA<<1)+4)
/*----------------------------------------------------------------------------*/
#ifndef IHEX_DATA_SIZE
#define IHEX_DATA_SIZE USE_IHEX_SIZE
#endif
#define IHEX_DATA_BUFF (HEX_DATA_CORE+IHEX_DATA_SIZE)
#define IHEX_LINE_SIZE ((IHEX_DATA_BUFF<<1)+4)
/*----------------------------------------------------------------------------*/
/* mininum chars in line */
#define IHEX_LINE_CMIN ((HEX_DATA_CORE<<1)+1)
#define IHEX_LINE_OFFS ((HEX_DATA_OFFS<<1)+1)
/*----------------------------------------------------------------------------*/
typedef unsigned char hexd_t;
/*----------------------------------------------------------------------------*/
typedef struct _my1ihex_data_t {
	hexd_t data[IHEX_DATA_SIZE];
	int addr, fill;
} my1ihex_data_t;
/*----------------------------------------------------------------------------*/
#define IHEXD(hd) ((my1ihex_data_t*)hd)
#define ihex_data_slot(hd) (IHEX_DATA_SIZE-IHEXD(hd)->fill)
#define ihex_data_curr(hd) (IHEXD(hd)->addr+IHEXD(hd)->fill)
/*----------------------------------------------------------------------------*/
typedef struct _my1ihex_t {
	unsigned int stat;
	int rows;
	my1ihex_data_t** init; /* dynamic pointer array */
} my1ihex_t;
/*----------------------------------------------------------------------------*/
#define IHEX(hx) ((my1ihex_t*)hx)
#define ihex_redo(hx) ihex_free(IHEX(hx)); ihex_init(IHEX(hx))
#define ihex_last(hx) IHEX(hx)->init[IHEX(hx)->rows-1]
/*----------------------------------------------------------------------------*/
void ihex_init(my1ihex_t* ihex);
void ihex_free(my1ihex_t* ihex);
int ihex_push(my1ihex_t* ihex, hexd_t* data, int size, int addr);
int ihex_write(my1ihex_t* ihex, char* name);
int ihex_read(my1ihex_t* ihex, char* name);
/*----------------------------------------------------------------------------*/
#endif /** __MY1IHEX_H__ */
/*----------------------------------------------------------------------------*/
