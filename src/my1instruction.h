/*----------------------------------------------------------------------------*/
#ifndef __MY1INSTRUCTION_H__
#define __MY1INSTRUCTION_H__
/*----------------------------------------------------------------------------*/
#include "my1types.h"
/*----------------------------------------------------------------------------*/
typedef struct _my1inst_t {
	byte08_t *data;
	int size, addr, line, lbsz;
	char* lbuf;
	struct _my1inst_t *next;
} my1inst_t;
/*----------------------------------------------------------------------------*/
void inst_init(my1inst_t* pcod, int size);
void inst_free(my1inst_t* pcod);
void inst_resize(my1inst_t* pcod, int size);
int inst_compare(my1inst_t *pcod, my1inst_t *qcod);
my1inst_t* inst_clone(my1inst_t* pcod);
/*----------------------------------------------------------------------------*/
#endif /** __MY1INSTRUCTION_H__ */
/*----------------------------------------------------------------------------*/
