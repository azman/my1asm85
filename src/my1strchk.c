/*----------------------------------------------------------------------------*/
#include "my1strchk.h"
/*----------------------------------------------------------------------------*/
#include <string.h>
#include <stdlib.h>
/*----------------------------------------------------------------------------*/
char is_whitespace(char that) {
	switch (that) {
		case ' ': case '\t': break;
		default: that = 0x0;
	}
	return that;
}
/*----------------------------------------------------------------------------*/
char is_listed(char that, char *list) {
	char pick = 0x0;
	while (*list) { /* browse through list of characters */
		if (that==(*list)) {
			pick = that; break;
		}
		list++;
	}
	return pick;
}
/*----------------------------------------------------------------------------*/
char is_alpha(char that) {
	if ((that<0x61||that>0x7a)&&
			(that<0x41||that>0x5a))
		that = 0x0; /* not an alphabet */
	return that;
}
/*----------------------------------------------------------------------------*/
char is_label_init(char achar) {
	if (!is_alpha(achar)&&
			achar!='$'&&achar!='_')
		achar = 0x0; /* not a label init */
	return achar;
}
/*----------------------------------------------------------------------------*/
char uppercase(char *that) {
	if (*that>=0x61&&*that<=0x7a)
		*that -= 0x20; /* make uppercase */
	return *that;
}
/*----------------------------------------------------------------------------*/
char* str2upper(char *pstr) {
	char *save = pstr;
	while (uppercase(pstr)) pstr++;
	return save;
}
/*----------------------------------------------------------------------------*/
char* trimws(char *pstr, int *trim, int norepeat) {
	int size, loop, step;
	char prev;
	/* get strlen */
	size = 0;
	while (pstr[size]) size++;
	/* trim right */
	size--;
	while (is_whitespace(pstr[size]))
		size--;
	size++;
	pstr[size] = 0x0;
	/* trim left */
	loop = 0;
	while (is_whitespace(pstr[loop]))
		loop++;
	/* copy, trim left & multiple sequence */
	for (step=0,prev=' ';loop<size;loop++) {
		if (norepeat&&is_whitespace(prev)&&is_whitespace(pstr[loop]))
			continue;
		pstr[step++] = pstr[loop];
		prev = pstr[loop];
	}
	/* terminate string */
	pstr[step] = 0x0;
	if (trim) *trim = step;
	return pstr;
}
/*----------------------------------------------------------------------------*/
char* get_operator(char *pstr, char *list, int *size) {
	char *pchk, *test;
	pchk = 0x0;
	/* browse through expression operators */
	while (*list) {
		test = strchr(pstr,*list);
		if (test) {
			if (!pchk) pchk = test;
			if (size) (*size)++;
		}
		list++;
	}
	return pchk;
}
/*----------------------------------------------------------------------------*/
char* check_quotes(char *pstr, int size) {
	int loop;
	char *pbuf;
	/* create buffer for processing */
	pbuf = malloc(size+1);
	for (loop=0;loop<size;loop++)
		pbuf[loop] = pstr[loop];
	pbuf[loop] = 0x0;
	trimws(pbuf,&size,0);
	/* check quotes */
	if (pbuf[0]=='"'&&pbuf[size-1]!='"') {
		free(pbuf);
		pbuf = 0x0;
	}
	return pbuf; /* returns malloc'ed buffer if quoted */
}
/*----------------------------------------------------------------------------*/
char* check_delims(char *pstr, char *list, int step) {
	int loop, flag, tcnt, size, slen;
	char *ptmp, *pbuf;
	flag = 0;
	tcnt = 0;
	size = 0;
	slen = 0;
	while (pstr[slen++]); /* get strlen */
	ptmp = pstr;
	pbuf = 0x0;
	/* traverse string */
	for (loop=0;loop<slen;loop++) {
		if (is_delim(pstr[loop],list)) {
			if (flag) {
				if (ptmp[0]=='"') { /* delim within quote! */
					if (!size||ptmp[size]!='"') {
						size++;
						continue;
					}
				}
				/* no char at all? */
				if (!size) break;
				/* create buffer */
				pbuf = check_quotes(ptmp,++size);
				if (!pbuf) continue; /* NOT a quote */
				/* is this what we're looking for? */
				if (tcnt==step) break;
				/* clean up */
				free(pbuf);
				pbuf = 0x0;
				/* find next token! */
				tcnt++;
				flag = 0;
			}
		}
		else {
			if (!flag) {
				ptmp = &pstr[loop];
				size = 0;
				flag = 1;
			}
			else size++;
		}
	}
	if (loop==slen&&flag&&tcnt==step) { /* last token? */
		if (pbuf) free((void*)pbuf); /* should NOT happen! */
		pbuf = check_quotes(ptmp,++size);
	}
	return pbuf;
}
/*----------------------------------------------------------------------------*/
