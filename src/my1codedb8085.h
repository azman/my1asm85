/*----------------------------------------------------------------------------*/
#ifndef __MY1CODEDB8085_H__
#define __MY1CODEDB8085_H__
/*----------------------------------------------------------------------------*/
#define MAX_OPCODE_CHAR 8
#define MAX_OPCODE_ARG 2
#define MAX_REGLBL_CHAR 4
#define REGCOUNT 8
#define RGPCOUNT 4
/*----------------------------------------------------------------------------*/
#define EXPR_OPS_NOT ' '
#define EXPR_OPS_ADD '+'
#define EXPR_OPS_SUB '-'
#define EXPR_OPS_MUL '*'
#define EXPR_OPS_DIV '/'
/*----------------------------------------------------------------------------*/
#define ARGTYPE_NONE 0
#define ARGTYPE_REG1 1
#define ARGTYPE_REG2 2
#define ARGTYPE_RGP1 3
#define ARGTYPE_RGP2 4
#define ARGTYPE_RGP3 5
#define ARGTYPE_IMM8 6
#define ARGTYPE_DI16 7
#define ARGTYPE_RSTX 8
#define ARGTYPE_EQUA 9
#define ARGTYPE_ADDR 10
#define ARGTYPE_DATA 11
#define ARGTYPE_STOR 12
#define ARGTYPE_ENDS 13
#define ARGTYPE_NOTI 14
#define ARGTYPE_EXTI 15
#define ARGTYPE_WORD 16
/*----------------------------------------------------------------------------*/
typedef struct _code8085_t {
	char instr[MAX_OPCODE_CHAR];
	int code, mask, size;
	int arg[MAX_OPCODE_ARG];
} code8085_t;
/*----------------------------------------------------------------------------*/
typedef struct _task8085_t {
	char direc[MAX_OPCODE_CHAR];
	int argtype;
} task8085_t;
/*----------------------------------------------------------------------------*/
const char REG8085[REGCOUNT][MAX_REGLBL_CHAR] = { "B", "C", "D", "E",
		"H", "L", "M", "A" };
const char RGP8085[REGCOUNT][MAX_REGLBL_CHAR] = { "B", "D", "H", "SP",
		"B", "D", "H", "PSW" };
const char EXPR_OPS[] = { EXPR_OPS_ADD, EXPR_OPS_SUB,
	EXPR_OPS_MUL, EXPR_OPS_DIV, 0x0 };
/*----------------------------------------------------------------------------*/
const code8085_t CodeDB[] = {
	{ "MOV", 0x40, 0xC0, 1, { ARGTYPE_REG1, ARGTYPE_REG2 } },
	{ "HLT", 0x76, 0xFF, 1, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "ADD", 0x80, 0xF8, 1, { ARGTYPE_REG2, ARGTYPE_NONE } },
	{ "ADC", 0x88, 0xF8, 1, { ARGTYPE_REG2, ARGTYPE_NONE } },
	{ "SUB", 0x90, 0xF8, 1, { ARGTYPE_REG2, ARGTYPE_NONE } },
	{ "SBB", 0x98, 0xF8, 1, { ARGTYPE_REG2, ARGTYPE_NONE } },
	{ "ANA", 0xA0, 0xF8, 1, { ARGTYPE_REG2, ARGTYPE_NONE } },
	{ "XRA", 0xA8, 0xF8, 1, { ARGTYPE_REG2, ARGTYPE_NONE } },
	{ "ORA", 0xB0, 0xF8, 1, { ARGTYPE_REG2, ARGTYPE_NONE } },
	{ "CMP", 0xB8, 0xF8, 1, { ARGTYPE_REG2, ARGTYPE_NONE } },
	{ "NOP", 0x00, 0xFF, 1, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "LXI", 0x01, 0xCF, 3, { ARGTYPE_RGP1, ARGTYPE_DI16 } },
	{ "STAX", 0x02, 0xCF, 1, { ARGTYPE_RGP3, ARGTYPE_NONE } },
	{ "INX", 0x03, 0xCF, 1, { ARGTYPE_RGP1, ARGTYPE_NONE } },
	{ "INR", 0x04, 0xC7, 1, { ARGTYPE_REG1, ARGTYPE_NONE } },
	{ "DCR", 0x05, 0xC7, 1, { ARGTYPE_REG1, ARGTYPE_NONE } },
	{ "MVI", 0x06, 0xC7, 2, { ARGTYPE_REG1, ARGTYPE_IMM8 } },
	{ "RLC", 0x07, 0xFF, 1, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "DAD", 0x09, 0xCF, 1, { ARGTYPE_RGP1, ARGTYPE_NONE } },
	{ "LDAX", 0x0A, 0xCF, 1, { ARGTYPE_RGP3, ARGTYPE_NONE } },
	{ "DCX", 0x0B, 0xCF, 1, { ARGTYPE_RGP1, ARGTYPE_NONE } },
	{ "RRC", 0x0F, 0xFF, 1, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "RAL", 0x17, 0xFF, 1, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "RAR", 0x1F, 0xFF, 1, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "RIM", 0x20, 0xFF, 1, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "SHLD", 0x22, 0xFF, 3, { ARGTYPE_DI16, ARGTYPE_NONE } },
	{ "DAA", 0x27, 0xFF, 1, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "LHLD", 0x2A, 0xFF, 3, { ARGTYPE_DI16, ARGTYPE_NONE } },
	{ "CMA", 0x2F, 0xFF, 1, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "SIM", 0x30, 0xFF, 1, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "STA", 0x32, 0xFF, 3, { ARGTYPE_DI16, ARGTYPE_NONE } },
	{ "STC", 0x37, 0xFF, 1, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "LDA", 0x3A, 0xFF, 3, { ARGTYPE_DI16, ARGTYPE_NONE } },
	{ "CMC", 0x3F, 0xFF, 1, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "RNZ", 0xC0, 0xFF, 1, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "POP", 0xC1, 0xCF, 1, { ARGTYPE_RGP2, ARGTYPE_NONE } },
	{ "JNZ", 0xC2, 0xFF, 3, { ARGTYPE_DI16, ARGTYPE_NONE } },
	{ "JMP", 0xC3, 0xFF, 3, { ARGTYPE_DI16, ARGTYPE_NONE } },
	{ "CNZ", 0xC4, 0xFF, 3, { ARGTYPE_DI16, ARGTYPE_NONE } },
	{ "PUSH", 0xC5, 0xCF, 1, { ARGTYPE_RGP2, ARGTYPE_NONE } },
	{ "ADI", 0xC6, 0xFF, 2, { ARGTYPE_IMM8, ARGTYPE_NONE } },
	{ "RST", 0xC7, 0xC7, 1, { ARGTYPE_RSTX, ARGTYPE_NONE } },
	{ "RZ", 0xC8, 0xFF, 1, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "RET", 0xC9, 0xCF, 1, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "JZ", 0xCA, 0xFF, 3, { ARGTYPE_DI16, ARGTYPE_NONE } },
	{ "CZ", 0xCC, 0xFF, 3, { ARGTYPE_DI16, ARGTYPE_NONE } },
	{ "CALL", 0xCD, 0xFF, 3, { ARGTYPE_DI16, ARGTYPE_NONE } },
	{ "ACI", 0xCE, 0xFF, 2, { ARGTYPE_IMM8, ARGTYPE_NONE } },
	{ "RNC", 0xD0, 0xFF, 1, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "JNC", 0xD2, 0xFF, 3, { ARGTYPE_DI16, ARGTYPE_NONE } },
	{ "OUT", 0xD3, 0xFF, 2, { ARGTYPE_IMM8, ARGTYPE_NONE } },
	{ "CNC", 0xD4, 0xFF, 3, { ARGTYPE_DI16, ARGTYPE_NONE } },
	{ "SUI", 0xD6, 0xFF, 2, { ARGTYPE_IMM8, ARGTYPE_NONE } },
	{ "RC", 0xD8, 0xFF, 1, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "JC", 0xDA, 0xFF, 3, { ARGTYPE_DI16, ARGTYPE_NONE } },
	{ "IN", 0xDB, 0xFF, 2, { ARGTYPE_IMM8, ARGTYPE_NONE } },
	{ "CC", 0xDC, 0xFF, 3, { ARGTYPE_DI16, ARGTYPE_NONE } },
	{ "SBI", 0xDE, 0xFF, 2, { ARGTYPE_IMM8, ARGTYPE_NONE } },
	{ "RPO", 0xE0, 0xFF, 1, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "JPO", 0xE2, 0xFF, 3, { ARGTYPE_DI16, ARGTYPE_NONE } },
	{ "XTHL", 0xE3, 0xFF, 1, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "CPO", 0xE4, 0xFF, 3, { ARGTYPE_DI16, ARGTYPE_NONE } },
	{ "ANI", 0xE6, 0xFF, 2, { ARGTYPE_IMM8, ARGTYPE_NONE } },
	{ "RPE", 0xE8, 0xFF, 1, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "PCHL", 0xE9, 0xFF, 1, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "JPE", 0xEA, 0xFF, 3, { ARGTYPE_DI16, ARGTYPE_NONE } },
	{ "XCHG", 0xEB, 0xFF, 1, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "CPE", 0xEC, 0xFF, 3, { ARGTYPE_DI16, ARGTYPE_NONE } },
	{ "XRI", 0xEE, 0xFF, 2, { ARGTYPE_IMM8, ARGTYPE_NONE } },
	{ "RP", 0xF0, 0xFF, 1, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "JP", 0xF2, 0xFF, 3, { ARGTYPE_DI16, ARGTYPE_NONE } },
	{ "DI", 0xF3, 0xFF, 1, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "CP", 0xF4, 0xFF, 3, { ARGTYPE_DI16, ARGTYPE_NONE } },
	{ "ORI", 0xF6, 0xFF, 2, { ARGTYPE_IMM8, ARGTYPE_NONE } },
	{ "RM", 0xF8, 0xFF, 1, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "SPHL", 0xF9, 0xFF, 1, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "JM", 0xFA, 0xFF, 3, { ARGTYPE_DI16, ARGTYPE_NONE } },
	{ "EI", 0xFB, 0xFF, 1, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "CM", 0xFC, 0xFF, 3, { ARGTYPE_DI16, ARGTYPE_NONE } },
	{ "CPI", 0xFE, 0xFF, 2, { ARGTYPE_IMM8, ARGTYPE_NONE } }
/*
 * unused opcode encoding
 *
	{ "XX0", 0x08, 0x00, 0, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "XX1", 0x10, 0x00, 0, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "XX2", 0x18, 0x00, 0, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "XX3", 0x28, 0x00, 0, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "XX4", 0x38, 0x00, 0, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "XX5", 0xCB, 0x00, 0, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "XX6", 0xD9, 0x00, 0, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "XX7", 0xDD, 0x00, 0, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "XX8", 0xED, 0x00, 0, { ARGTYPE_NONE, ARGTYPE_NONE } },
	{ "XX9", 0xFD, 0x00, 0, { ARGTYPE_NONE, ARGTYPE_NONE } }
*/
};
/*----------------------------------------------------------------------------*/
const task8085_t TaskDB[] = {
	{ "CPU", ARGTYPE_NOTI },
	{ "ORG", ARGTYPE_ADDR },
	{ "END", ARGTYPE_ENDS },
	{ "ENDS", ARGTYPE_ENDS },
	{ "DFB", ARGTYPE_DATA },
	{ "DFS", ARGTYPE_STOR },
	{ "DFW", ARGTYPE_WORD },
	{ "DB", ARGTYPE_DATA },  /* also supports this syntax! */
	{ "DS", ARGTYPE_STOR },  /* also supports this syntax! */
	{ "DW", ARGTYPE_WORD },
	{ "EQU", ARGTYPE_EQUA },
	{ "EXT", ARGTYPE_EXTI } /* customizable external directive */
};
/*----------------------------------------------------------------------------*/
const int CODEDBSIZE = sizeof(CodeDB)/sizeof(code8085_t);
const int TASKDBSIZE = sizeof(TaskDB)/sizeof(task8085_t);
/*----------------------------------------------------------------------------*/
#endif /** __MY1CODEDB8085_H__ */
/*----------------------------------------------------------------------------*/
