/*----------------------------------------------------------------------------*/
#include "my1ihex.h"
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#define STAT_OK 0
#define STAT_ERROR (~(~0U>>1))
#define STAT_ERROR_ARGS (STAT_ERROR|0x01)
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	char *psrc, *pdst, *ptmp;
	my1ihex_t ihex;
	unsigned int stat;
	int loop;
	/* initialize */
	psrc = 0x0; pdst = 0x0; stat = 0;
	ihex_init(&ihex);
	for (loop=1;loop<argc;loop++) {
		if (argv[loop][0]=='-') {
			ptmp = &argv[loop][1];
			if (!strncmp(ptmp,"i",2)) {
				if (++loop<argc) psrc = argv[loop];
				else {
					printf("** No value for '%s'!\n",argv[loop-1]);
					stat |= STAT_ERROR_ARGS;
				}
			}
			else if (!strncmp(ptmp,"o",2)) {
				if (++loop<argc) pdst = argv[loop];
				else {
					printf("** No value for '%s'!\n",argv[loop-1]);
					stat |= STAT_ERROR_ARGS;
				}
			}
		}
		else psrc = argv[loop]; /* assume input */
	}
	do {
		if (!psrc||stat) break;
		/* read input */
		loop = ihex_read(&ihex,psrc);
		if (loop) {
			printf("** Read error! (%s)[%d]\n",psrc,loop);
			break;
		}
		/* show output */
		loop = ihex_write(&ihex,pdst);
		if (loop) printf("** Write error! (%s)[%d]\n",pdst,loop);
		else if (pdst) printf("@@ HEX file written! (%s)\n",pdst);
	} while (0);
	ihex_free(&ihex);
	return stat;
}
/*----------------------------------------------------------------------------*/
