/*----------------------------------------------------------------------------*/
#ifndef __MY1CODE85_H__
#define __MY1CODE85_H__
/*----------------------------------------------------------------------------*/
#include "my1code.h"
#include "my1parser.h"
/*----------------------------------------------------------------------------*/
#define MAX_8085_ADDR 0xFFFF
/*----------------------------------------------------------------------------*/
#define SYNTAX_OK 0
#define SYNTAX_INVALID_OPCODE -1
#define SYNTAX_INVALID_PARAM -2
#define SYNTAX_NO_PARAM -3
#define SYNTAX_EXTRA_PARAM -4
#define SYNTAX_INVALID_LABEL -5
#define SYNTAX_UNKNOWN_LABEL -6
#define SYNTAX_NO_LABEL -7
#define SYNTAX_CHECK_LABEL -8
#define SYNTAX_CHECK_DIRECTIVE -9
#define SYNTAX_MEMORY_ERROR -10
#define SYNTAX_CHECK_EXPR -11
#define SYNTAX_NOT_STR -12
#define SYNTAX_INVALID_OPS -13
#define SYNTAX_NOT_EXPR -14
#define SYNTAX_DUPLICATE_LABEL -15
#define SYNTAX_OVERLAPPED_MEMORY -16
#define SYNTAX_MUST_RESOLVE_ADDR -17
/*----------------------------------------------------------------------------*/
#define INDEX_OPCODE 0
#define INDEX_IMM08 1
#define INDEX_IMM16 2
#define MAX_CODEBYTE 3
/*----------------------------------------------------------------------------*/
#define MAX_LINES_CHAR 256
#define MAX_ARG_CHECK (MAX_OPCODE_ARG+1)
#define MAX_TOK_CHECK 2
/*----------------------------------------------------------------------------*/
/* disable error printouts */
#define CODE85_FLAG_QUIET 0x08
/*----------------------------------------------------------------------------*/
typedef void (*code85task_t)(void*);
/*----------------------------------------------------------------------------*/
typedef struct _my1code85_t {
	my1code_t code;
	my1parser_t read;
	code85task_t ccpy; /* code copy function */
	code85task_t echk; /* on parser 'error' */
	void* eobj; /* to be used by echk */
	void* temp; /* used by code2lst */
	void* show; /* output redirect */
	char *pcmd, *parg; /* temporary for ccpy */
	word32_t flag, stat;
} my1code85_t;
/*----------------------------------------------------------------------------*/
#define code85_show(pc,pf) ((my1code85_t*)pc)->show = pf
#define code85_ccpy(pc,cc) ((my1code85_t*)pc)->ccpy = (code85task_t)cc
#define code85_echk(pc,ef) ((my1code85_t*)pc)->echk = (code85task_t)ef
#define code85_eobj(pc,eo) ((my1code85_t*)pc)->eobj = (void*)eo
#define code85_doquiet(pc) ((my1code85_t*)pc)->flag |= CODE85_FLAG_QUIET
#define code85_mainapp(pc) ((my1code85_t*)pc)->flag &= ~CODE85_FLAG_QUIET
/*----------------------------------------------------------------------------*/
void code85_init(my1code85_t* that);
void code85_free(my1code85_t* that);
int code85_read(my1code85_t* that, char* name);
/*----------------------------------------------------------------------------*/
#endif /** __MY1CODE85_H__ */
/*----------------------------------------------------------------------------*/
