/*----------------------------------------------------------------------------*/
#ifndef __MY1INST2HEX_H__
#define __MY1INST2HEX_H__
/*----------------------------------------------------------------------------*/
#include "my1instruction.h"
#include "my1ihex.h"
/*----------------------------------------------------------------------------*/
int inst_2hex(my1inst_t *pcod, char *file) {
	int stat, addr;
	my1ihex_t ihex;
	stat = 0; addr = -1;
	ihex_init(&ihex);
	while (pcod) {
		if (pcod->line>=0) {
			if (pcod->addr<addr) {
				stat--;
				break;
			}
			else if (pcod->addr>addr) {
				addr = pcod->addr;
			}
			if (ihex_push(&ihex,pcod->data,pcod->size,addr)!=pcod->size)
				stat--;
			addr += pcod->size;
		}
		pcod = pcod->next;
	}
	if (ihex_write(&ihex,file)) stat--;
	ihex_free(&ihex);
	return stat;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1INST2HEX_H__ */
/*----------------------------------------------------------------------------*/
