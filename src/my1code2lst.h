/*----------------------------------------------------------------------------*/
#ifndef __MY1CODE2LST_H__
#define __MY1CODE2LST_H__
/*----------------------------------------------------------------------------*/
#include "my1code85.h"
/*----------------------------------------------------------------------------*/
#include <stdio.h>
/*----------------------------------------------------------------------------*/
#define LBL_COL_PER_ROW 4
/*----------------------------------------------------------------------------*/
/** should be run on pass 2 in line parser */
void list_write(FILE *plst, my1code_t *code, char *buff) {
	my1symb_t *psym;
	int loop;
	if (!buff) {
		fprintf(plst,"\n\n[LABELS]");
		fprintf(plst,"\n--------\n");
		psym = code->psym;
		while (psym) {
			for (loop=0;loop<LBL_COL_PER_ROW;loop++) {
				if (loop) fprintf(plst,"\t\t");
				fprintf(plst,"%04X = '%s'",psym->data,psym->name);
				psym = psym->next;
				if (!psym) break;
			}
			fprintf(plst,"\n");
		}
		return;
	}
	if (code->ccod) {
		fprintf(plst,"[C]%04X ",code->ccod->addr);
		for (loop=0;loop<code->ccod->size;loop++)
			fprintf(plst,"%02X",code->ccod->data[loop]);
		for (loop*=2;loop<16;loop++)
			fprintf(plst," ");
	}
	else if(code->csym) {
		fprintf(plst, "[L]%04X", code->csym->addr);
		if (code->csym->data!=code->csym->addr)
			fprintf(plst," %04X",code->csym->data);
		else
			fprintf(plst,"     ");
		for (loop=5;loop<17;loop++)
			fprintf(plst," ");
	}
	else {
		fprintf(plst, "[O]%04X", code->addr);
		for (loop=0;loop<17;loop++)
			fprintf(plst," ");
	}
	/* simply re-print the line */
	fprintf(plst,"%s\n", buff);
}
/*----------------------------------------------------------------------------*/
int code85_lister(my1parser_t *parser) {
	my1code85_t *ptop;
	my1code_t *code;
	FILE *plst;
	ptop = (my1code85_t*)parser->encode;
	code = &ptop->code;
	plst = (FILE*)ptop->temp;
	list_write(plst,code,parser->rbuf);
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1CODE2LST_H__ */
/*----------------------------------------------------------------------------*/
